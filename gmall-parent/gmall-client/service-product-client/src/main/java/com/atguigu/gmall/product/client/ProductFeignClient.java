package com.atguigu.gmall.product.client;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.client.impl.ProductDegradeFeignClient;
import com.atguigu.gmall.product.model.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @FeignClient 通过value=指定调用某个目标服务的名称，要和nacos注册服务名称一致，
 * path="url基础地址"
 * fallback指定服务降级字节码，上游服务是提供方，为了保护下游的服务，上游的服务出现问题不能影响到下游的服务
 * 这个接口便是服务的提供方
 */
@FeignClient(value = "service-product", fallback = ProductDegradeFeignClient.class)  // http://service-product
public interface ProductFeignClient {

    /**
     * 根据SkuId查询商品信息-包含图片列表
     * 如果不想再用Result.data取数据，可以直接使用SkuInfo返回信息
     * 仅适用于两个服务间进行调用，但是给前端返回数据需要使用Result
     * @param skuId
     * @return
     * 类上的地址不能忘记复制/api/product
     */
    @GetMapping("/api/product/inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable("skuId") Long skuId);


    /**
     * 根据三级分类Id获取分类信息
     * @param category3Id
     * @return
     */
    @GetMapping("/api/product/inner/getCategoryView/{category3Id}")
    public BaseCategoryView getCategoryView(@PathVariable("category3Id") Long category3Id);


    /**
     * 根据SKuId查询商品最新价格
     */
    @GetMapping("/api/product/inner/getSkuPrice/{skuId}")
    public BigDecimal getSkuPrice(@PathVariable("skuId") Long skuId);


    /**
     * 根据商品SpuId查询海报图片列表
     * @param spuId
     * @return
     */
    @GetMapping("/api/product/inner/findSpuPosterBySpuId/{spuId}")
    public List<SpuPoster> getSpuPosterBySpuId(@PathVariable("spuId") Long spuId);



    /**
     * 根据skuId查询当前SKU商品包含平台属性列表（包含属性值）
     * @return
     */
    @GetMapping("/api/product/inner/getAttrList/{skuId}")
    public List<BaseAttrInfo> getAttrList(@PathVariable("skuId") Long skuId);


    /**
     *  查询spu商品所有销售属性，查询指定sku销售属性选中效果
     * @param skuId
     * @param spuId
     * @return
     */
    @GetMapping("/api/product/inner/getSpuSaleAttrListCheckBySku/{skuId}/{spuId}")
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(
            @PathVariable("skuId") Long skuId,
            @PathVariable("spuId") Long spuId);


    /**
     * 在一组SPU商品下 切换不同SKU字符串
     * @param spuId "{'销售属性1|销售属性2':'sku商品ID','销售属性1|销售属性3':'sku商品ID'}"
     * @return
     */
    @GetMapping("/api/product/inner/getSkuValueIdsMap/{spuId}")
    public String getSkuValueIdsMap(@PathVariable("spuId") Long spuId);


    /**
     * 查询所有1级分类列表，要求将二级分类包含到一级分类下，
     * 三级分类包含到二级分类下
     * @return
     */
    @GetMapping("/api/product/inner/getBaseCategoryList")
    public List<JSONObject> getBaseCategoryList();

    /**
     *根据品牌的id查询品牌的信息
     * @param tmId
     * @return
     */
    @GetMapping("/api/product/inner/getTrademark/{tmId}")
    public BaseTrademark getBaseTrademarkById(@PathVariable("tmId") Long tmId);
}