package com.atguigu.gmall.product.client.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.client.ProductFeignClient;
import com.atguigu.gmall.product.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
@Component
@Slf4j
public class ProductDegradeFeignClient implements ProductFeignClient {
    /**
     * 根据SkuId查询商品信息-包含图片列表
     * 如果不想再用Result.data取数据，可以直接使用SkuInfo返回信息
     * 仅适用于两个服务间进行调用，但是给前端返回数据需要使用Result
     * @param skuId
     * @return
     */
    @Override
    public SkuInfo getSkuInfo(Long skuId) {
        log.error("[远程调用商品模块getSkuInfo异常，服务降级方法执行]");
        return null;
    }
    /**
     * 根据三级分类Id获取分类信息
     * @param category3Id
     * @return
     */
    @Override
    public BaseCategoryView getCategoryView(Long category3Id) {
        log.error("[远程调用商品模块getCategoryView异常，服务降级方法执行]");
        return null;
    }
    /**
     * 根据SKuId查询商品最新价格
     */
    @Override
    public BigDecimal getSkuPrice(Long skuId) {
        log.error("[远程调用商品模块getSkuPrice异常，服务降级方法执行]");
        return null;
    }
    /**
     * 根据商品SpuId查询海报图片列表
     * @param spuId
     * @return
     */
    @Override
    public List<SpuPoster> getSpuPosterBySpuId(Long spuId) {
        log.error("[远程调用商品模块getSpuPosterBySpuId异常，服务降级方法执行]");
        return null;
    }
    /**
     * 根据skuId查询当前SKU商品包含平台属性列表（包含属性值）
     * @return
     */
    @Override
    public List<BaseAttrInfo> getAttrList(Long skuId) {
        log.error("[远程调用商品模块getAttrList异常，服务降级方法执行]");
        return null;
    }
    /**
     *  查询spu商品所有销售属性，查询指定sku销售属性选中效果
     * @param skuId
     * @param spuId
     * @return
     */
    @Override
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId) {
        log.error("[远程调用商品模块getSpuSaleAttrListCheckBySku异常，服务降级方法执行]");
        return null;
    }

    @Override
    public String getSkuValueIdsMap(Long spuId) {
        log.error("[远程调用商品模块getSkuValueIdsMap异常，服务降级方法执行]");
        return null;
    }
    /**
     * 查询所有1级分类列表，要求将二级分类包含到一级分类下，
     * 三级分类包含到二级分类下
     * @return
     */
    @Override  
    public List<JSONObject> getBaseCategoryList() {
        log.error("[远程调用商品模块getBaseCategoryList异常，服务降级方法执行]");
        return null;
    }
    /**
     *根据品牌的id查询品牌的信息
     * @param tmId
     * @return
     */
    @Override
    public BaseTrademark getBaseTrademarkById(Long tmId) {
        log.error("[远程调用商品模块getBaseTrademarkById异常，服务降级方法执行]");
        return null;
    }
}
