package com.atguigu.gmall.impl;

import com.atguigu.gmall.ActivityFeignClient;
import com.atguigu.gmall.common.result.Result;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Component
public class ActivityFeignClientImpl implements ActivityFeignClient {
    @Override
    public Result getSeckillGoods() {
        return null;
    }

    @Override
    public Result getSecGoodsById(Long skuId) {
        return null;
    }

    @Override
    public Result getSeckillSkuIdStr(HttpServletRequest request, Long skuId) {
        return null;
    }

    @Override
    public Result seckillReq2Queue(HttpServletRequest request, Long skuId, String buyCode) {
        return null;
    }

    @Override
    public Result checkSeckillResult(HttpServletRequest request, Long skuId) {
        return null;
    }

    @Override
    public Result<Map> seckillOrderTrade(Long skuId) {
        return null;
    }
}
