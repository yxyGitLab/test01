package com.atguigu.gmall;

import com.atguigu.gmall.activity.model.SeckillGoods;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.impl.ActivityFeignClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@FeignClient(value = "service-activity",path="/api/activity",fallback = ActivityFeignClientImpl.class)
public interface ActivityFeignClient {


    /**
     * 查询当日参与秒杀商品
     *
     * @return
     */
    @GetMapping("/seckill/findAll")
    public Result getSeckillGoods();


    /**
     * 查询秒杀商品详情  从redis获取
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/getSeckillGoods/{skuId}")
    public Result getSecGoodsById(@PathVariable("skuId") Long skuId);

    /**
     * 获取抢购码
     *
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/auth/getSeckillSkuIdStr/{skuId}")
    public Result getSeckillSkuIdStr(HttpServletRequest request, @PathVariable("skuId") Long skuId);

    /**
     * 秒杀预下单,业务校验通过后,将秒杀请求放入消息队列,不等待秒杀业务处理立即返回
     * @param request
     * @param skuId 秒杀商品ID
     * @param buyCode 抢购码
     * @return
     */
    @PostMapping("/seckill/auth/seckillOrder/{skuId}")
    public Result seckillReq2Queue(HttpServletRequest request, @PathVariable("skuId") Long skuId, @RequestParam("skuIdStr") String buyCode);


    /**
     * 检查用户秒杀下单结果
     * @param request
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/auth/checkOrder/{skuId}")
    public Result<Map> checkSeckillResult(HttpServletRequest request, @PathVariable("skuId") Long skuId);

    /**
     * 渲染秒杀订单确认页面相关数据
     * @param
     * @return
     */
    @GetMapping("/seckill/auth/trade/{skuId}")
    public Result<Map> seckillOrderTrade(@PathVariable("skuId") Long skuId);
}
