package com.atguigu.gmall.list.client;


import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.list.client.impl.ListDegradeFeignClient;
import com.atguigu.gmall.list.model.SearchParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(value = "service-list", fallback = ListDegradeFeignClient.class)
public interface ListFeignClient {

    /**
     * 提供给详情服务调用:更新商品热度分值
     * @param skuId
     */
    @GetMapping("/api/list/inner/incrHotScore/{skuId}")
    public void incrHotScore(@PathVariable("skuId") Long skuId);

    /**
     * 提供给前端服务/移动端,商品检索
     * 泛型将原来SearchResponseVo对象修改为Map Feigh内部会自动转换，方便渲染页面
     * @param searchParam
     * @return
     */
    @PostMapping({"/api/list"})
    public Result<Map> search(@RequestBody SearchParam searchParam);

}
