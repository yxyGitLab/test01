package com.atguigu.gmall.list.client.impl;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.list.client.ListFeignClient;
import com.atguigu.gmall.list.model.SearchParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class ListDegradeFeignClient implements ListFeignClient {
    @Override
    public void incrHotScore(Long skuId) {
        log.error("[搜索服务Feign]远程调用incrHotScore方法异常:");
    }

    @Override
    public Result<Map> search(SearchParam searchParam) {
        log.error("[搜索服务Feign]远程调用search方法异常:");
        return null;
    }
}
