package com.atguigu.gmall.item.client.impl;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.item.client.ItemFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ItemDegradeFeignClient implements ItemFeignClient {
    @Override
    public Result getItemInfo(Long skuId) {
        log.error("[详情服务]执行了服务降级方法");
        return null;
    }
}