package com.atguigu.gmall.user.client.impl;

import com.atguigu.gmall.user.client.UserFeignClient;
import com.atguigu.gmall.user.model.UserAddress;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class UserDegradeFeignClient implements UserFeignClient {

    @Override
    public List<UserAddress> getUserAddressListByUserId(Long userId) {
        log.error("[用户服务]Feign调用getUserAddressListByUserId方法服务降级");
        return null;
    }
}
