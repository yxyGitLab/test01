package com.atguigu.gmall.order.client;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.order.client.impl.OrderDegradeFeignClient;
import com.atguigu.gmall.order.model.OrderInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(value = "service-order", path = "/api/order", fallback = OrderDegradeFeignClient.class)
public interface OrderFeignClient {

    /**
     * 渲染用户订单确认页面相关参数
     * TODO:Feign远程调用是新的请求,方法参数中不能携带请求对象,远程调用封装失败
     * 目标方法即使有HttpServletRequest对象 ,也不是同一个请求对象
     * @return
     */
    @GetMapping("/auth/trade")
    public Result<Map> orderTradeData();

    /**
     * 查询订单信息(包含订单明细)
     * @param orderId
     * @return
     */
    @GetMapping("/inner/getOrderInfo/{orderId}")
    public OrderInfo getOrderInfo(@PathVariable("orderId") Long orderId);

    /**
     * 保存秒杀订单
     * @param orderInfo
     * @return
     */
    @PostMapping("/inner/seckill/submitOrder")
    public Long submitSeckillOrder(@RequestBody OrderInfo orderInfo);
}