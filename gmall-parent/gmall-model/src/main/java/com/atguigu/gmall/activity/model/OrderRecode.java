package com.atguigu.gmall.activity.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 秒杀资格获取后,生成的临时订单
 */
@Data
public class OrderRecode implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userId;

	private SeckillGoods seckillGoods;

	private Integer num;

	private String orderStr;
}
