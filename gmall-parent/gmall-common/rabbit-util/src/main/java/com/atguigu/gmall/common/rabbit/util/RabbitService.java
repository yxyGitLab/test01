package com.atguigu.gmall.common.rabbit.util;

import com.alibaba.fastjson.JSON;
import com.atguigu.gmall.common.rabbit.model.GmallCorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class RabbitService {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送消息到MQ服务器工具方法
     * @param exchange  交换机
     * @param routingKey 路由键
     * @param obj 业务数据
     * @return
     */

    /**
     * 用于生产者发送消息方法
     */
    public void sendMessage(String exchange, String routingKey, Object msg) {
        //调用模板对象发送消息
        //1.构建相关数据,将相关数据存入Redis
        GmallCorrelationData correlationData = new GmallCorrelationData();
        //相关数据唯一标识
        String uuid = "mq:" + UUID.randomUUID().toString().replaceAll("-", "");
        correlationData.setId(uuid);
        correlationData.setExchange(exchange);
        correlationData.setRoutingKey(routingKey);
        correlationData.setMessage(msg);
        //存入redis
        redisTemplate.opsForValue().set(uuid, JSON.toJSONString(correlationData), 1, TimeUnit.HOURS);
        //2.发送消息时候 选择既有业务消息还包括相关消息
        rabbitTemplate.convertAndSend(exchange, routingKey, msg, correlationData);
    }
    /**
     * 发送延迟消息-采用延迟插件实现
     *
     * @param
     * @param delayTime 延迟时间 单位:s
     * @return
     */
    public boolean sendDelayMessage(String exchange, String routingKey, Object obj, Integer delayTime) {
        GmallCorrelationData correlationData = new GmallCorrelationData();
        String id = UUID.randomUUID().toString().replace("-", "");
        correlationData.setId(id);
        correlationData.setMessage(obj);
        correlationData.setExchange(exchange);
        correlationData.setRoutingKey(routingKey);
        correlationData.setDelay(true);
        correlationData.setDelayTime(delayTime);
        //2.将相关数据存入Redis
        String key = "mq:" + id;
        redisTemplate.opsForValue().set(key, JSON.toJSONString(correlationData), 5, TimeUnit.MINUTES);
        rabbitTemplate.convertAndSend(exchange, routingKey, obj, (message) -> {
            message.getMessageProperties().setDelay(delayTime * 1000);
            return message;
        }, correlationData);
        return true;
    }
    
}
