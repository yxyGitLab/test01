package com.atguigu.gmall.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class ThreadPoolConfig {
//配置自定义线程池
    @Bean
    public ThreadPoolExecutor threadPoolExecutor(){
        int processorsCount = Runtime.getRuntime().availableProcessors();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                processorsCount * 2,
                processorsCount * 2,
                0,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(200),
                Executors.defaultThreadFactory(),
                //new ThreadPoolExecutor.CallerRunsPolicy()
                (runnable, executor) -> {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                    }
                    executor.submit(runnable);
                }
        );
        //线程池创建,核心线程同时创建,为了提高线程池的效率
        threadPoolExecutor.prestartCoreThread();
        //threadPoolExecutor.prestartAllCoreThreads();
        return threadPoolExecutor;
    }
}
