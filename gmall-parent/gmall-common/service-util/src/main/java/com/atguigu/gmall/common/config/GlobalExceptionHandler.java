package com.atguigu.gmall.common.config;

import com.atguigu.gmall.common.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 必须保证配置类能扫描到
 */
@Slf4j
@Configuration
@RestControllerAdvice//对controller层进行增强
public class GlobalExceptionHandler {
    /**
     * 处理运行时异常
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public Result runtimeException(RuntimeException e){
        log.error("全局异常处理:{}"+e);
        return Result.fail().message(e.getMessage());
    }
    /**
     * 处理运行时异常
     * @return
     */
    @ExceptionHandler(Exception.class)
    public Result Exception(Exception e){
        log.error("全局异常处理:{}"+e);
        return Result.fail().message(e.getMessage());
    }
}
