package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.list.client.ListFeignClient;
import com.atguigu.gmall.list.model.SearchAttr;
import com.atguigu.gmall.list.model.SearchParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 产品列表接口
 */
@Controller
public class ListHtmlController {
    @Autowired
    private ListFeignClient listFeignClient;

    /**
     * 渲染搜索页面
     * http://list.gmall.com/list.html?keyword=手机&pageNo=2&order=1:asc&props=23:8G:运行内存&props=24:128G:机身存储
     *
     * @param model
     * @return
     */
    @GetMapping("/list.html")
    public String listHtml(Model model, SearchParam searchParam) {
        //1.远程调用搜索服务进行检索
        Result<Map> result = listFeignClient.search(searchParam);

        //2.为渲染页面模板进行赋值
        model.addAllAttributes(result.getData());

        //3.以下所有参数本应该由前端记录提交提交
        //3.1 用户提交请求参数 ${searchParam}
        model.addAttribute("searchParam", searchParam);

        //3.2 用户回显地址栏中请求 包含搜索条件完整链接 ${urlParam}
        model.addAttribute("urlParam", this.makeUrlParam(searchParam));

        //3.3 回显用户已选中品牌面包屑 ${trademarkParam}
        model.addAttribute("trademarkParam", this.makeTrademarkParam(searchParam.getTrademark()));

        //3.4 回显用户已选中平台属性 ${propsParamList}
        model.addAttribute("propsParamList", this.mekePropsParamList(searchParam.getProps()));

        //3.5 回显页面排序效果${orderMap}   type:排序方式 sort:升降序
        model.addAttribute("orderMap", this.makeOrderMap(searchParam.getOrder()));

        return "list/index";
    }

    /**
     * 根据用户已选择平台属性,封装集合对象用户回显平台属性面包屑
     *
     * @param props
     * @return
     */
    private List<SearchAttr> mekePropsParamList(String[] props) {
        if (props != null && props.length > 0) {
            List<String> propsList = Arrays.asList(props);
            List<SearchAttr> searchAttrList = propsList.stream().map(prop -> {
                SearchAttr searchAttr = new SearchAttr();
                //形式   属性id:属性值:属性名称
                String[] split = prop.split(":");
                if (split != null && split.length == 3) {
                    searchAttr.setAttrId(Long.valueOf(split[0]));
                    searchAttr.setAttrValue(split[1]);
                    searchAttr.setAttrName(split[2]);
                }
                return searchAttr;
            }).collect(Collectors.toList());
            return searchAttrList;
        }
        return null;
    }

    /**
     * 用于回显用户选中品牌面包屑  品牌：用户选择的品牌名称
     * @param trademark
     * @return
     */
    private Object makeTrademarkParam(String trademark) {
        if (StringUtils.isNotBlank(trademark)) {
            String[] split = trademark.split(":");
            if (split != null && split.length == 2) {
                return "品牌: " + split[1];
            }
        }
        return null;
    }

    /**
     * 制作用户已选中的所有条件URL请求
     * @param searchParam
     * @return
     */
    private String makeUrlParam(SearchParam searchParam) {
        //1.在列表页面默认地址前置
        StringBuilder stringBuilder = new StringBuilder("/list.html?");
        if (StringUtils.isNotBlank(searchParam.getKeyword())) {
            stringBuilder.append("&keyword=" + searchParam.getKeyword());
        }
        if (StringUtils.isNotBlank(searchParam.getTrademark())) {
            stringBuilder.append("&trademark=" + searchParam.getTrademark());
        }
        if (searchParam.getCategory1Id() != null) {
            stringBuilder.append("&category1Id=" + searchParam.getCategory1Id());
        }
        if (searchParam.getCategory2Id() != null) {
            stringBuilder.append("&category2Id=" + searchParam.getCategory2Id());
        }
        if (searchParam.getCategory3Id() != null) {
            stringBuilder.append("&category3Id=" + searchParam.getCategory3Id());
        }
        String[] props = searchParam.getProps();
        if (props != null && props.length > 0) {
            for (String prop : props) {
                stringBuilder.append("&props=").append(prop);
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 根据用户选择排序方式 回显页面排序效果
     *
     * @param order 形式:     (综合降序)order=1:desc    (价格升序)order=2:asc
     * @return
     */
    private Map<String, String> makeOrderMap(String order) {
        HashMap<String, String> orderMap = new HashMap<>();
        if (StringUtils.isNotBlank(order)) {
            String[] split = order.split(":");
            if (split != null && split.length == 2) {
                orderMap.put("type", split[0]);
                orderMap.put("sort", split[1]);
                return orderMap;
            }
        }
        //默认排序方式 按照ES文档相关性算分进行排序
        orderMap.put("type", "3");
        orderMap.put("sort", "desc");
        return orderMap;
    }
}
