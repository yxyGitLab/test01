package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.order.client.OrderFeignClient;
import com.atguigu.gmall.order.model.OrderInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class OrderHtmlController {


    @Autowired
    private OrderFeignClient orderFeignClient;


    /**
     * 订单确认页面渲染
     * @param model
     * @return
     */
    @GetMapping("/trade.html")
    public String tradeHtml(Model model) {
        //1.远程调用订单服务得到汇总数据，得到相关数据
        Result<Map> result = orderFeignClient.orderTradeData();
        //2.渲染页面
        model.addAllAttributes(result.getData());
        //订单确认页面
        return "order/trade";
    }

    /**
     * 我的订单渲染
     * @return
     */
    @GetMapping("/myOrder.html")
    public String myOrderHtml(){
        return "/order/myOrder";
    }

}