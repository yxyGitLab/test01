package com.atguigu.gmall.web.controller;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.client.ProductFeignClient;
import com.atguigu.gmall.product.model.SkuInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 购物车页面显示控制器
 */
@Controller
public class CartHtmlController {
    //远程调用商品服务获得商品信息
     @Autowired
     private ProductFeignClient productFeignClient;
    /**
     * 添加商品购物车成功页面
     * @param model
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("addCart.html")
    public String addCartSuccess(Model model,
                              @RequestParam("skuId") Long skuId,
                              @RequestParam("skuNum") Integer num )  {
        //1.远程获取商品的信息
        SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
        model.addAttribute("skuInfo",skuInfo);
        model.addAttribute("skuNum",num);
        return "cart/addCart";
    }

    /**
     * 渲染购物车的列表页面
     * @return
     */
    @GetMapping("/cart.html")
    public String cartHtml(){
        return "/cart/index";
    }
}
