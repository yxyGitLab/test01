package com.atguigu.gmall.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.product.client.ProductFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexHtmlController {
    /**
     * 商品模块的远程调用
     */
    @Autowired
    private ProductFeignClient productFeignClient;
    /**
     * 渲染首页
     * @param model
     * @return
     */
    @GetMapping({"/",""})
    public String indexHtml(Model model){
        //1.远程调用商品模块获取分类列表
        List<JSONObject> baseCategoryList = productFeignClient.getBaseCategoryList();
        //  首页中其他页面渲染数据 来源其他微服务
        // 调用内存推荐服务得到推荐商品列表
        //调用订单服务得到热门下单商品
        //调用广告服务获取广告列表

        //2.给模型赋值
        model.addAttribute("list",baseCategoryList);
        //3.渲染页面模板
        return "/index/index";
    }

}
