package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.item.client.ItemFeignClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@Controller
public class ItemHtmlController {

    @Autowired
    private ItemFeignClient itemFeignClient;


    /**
     * 渲染商品详情页
     *
     * @param skuId
     * @return
     */
    @GetMapping("/{skuId}.html")
    public String itemHtml(Model model, @PathVariable("skuId") Long skuId) {
        //1.远程调用详情服务获取所有数据
        Result<Map> result = itemFeignClient.getItemInfo(skuId);
        //2.将获取到数据封装到Model中,Thymeleaf底层使用模板引擎将数据跟模板页面进行合并 生成静态页
        model.addAllAttributes(result.getData());
        return "/item/item";
    }
}