package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.common.result.Result;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginHtmlController {

    /**
     * 渲染登录页面
     * @param model
     * @param originUrl
     * @return
     * 加了@Requestparam后要求这个此参数必须填写，一般用在要求必须填写的位置
     */
    @GetMapping("/login.html")
    public String loginHtml(Model model, @RequestParam(value = "originUrl", required = false) String originUrl) {
        //originUrl是用户访问上一个域名的地址
        model.addAttribute("originUrl", originUrl);
        return "login";
    }

}