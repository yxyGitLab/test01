package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.order.client.OrderFeignClient;
import com.atguigu.gmall.order.model.OrderInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PaymentHtmlController {

    @Autowired
    private OrderFeignClient orderFeignClient;

    /**
     *渲染选择支付页面（支付宝或微信或其他）
     */
    @GetMapping("/pay.html")
    public String choosePayHtml(Model model, @RequestParam("orderId") Long orderId){
        //1.远程调用订单服务获取订单信息
        OrderInfo orderInfo = orderFeignClient.getOrderInfo(orderId);
        //2.设置数据
        model.addAttribute("orderInfo",orderInfo);
        return  "payment/pay";
    }

    /**
     * 支付成功页面渲染
     * @return
     */
    @GetMapping("/pay/success.html")
    public String paySuccessHtml() {
        return "/payment/success";
    }
}
