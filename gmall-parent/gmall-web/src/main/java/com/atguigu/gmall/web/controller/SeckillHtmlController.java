package com.atguigu.gmall.web.controller;

import com.atguigu.gmall.ActivityFeignClient;
import com.atguigu.gmall.common.result.Result;
import org.bouncycastle.math.raw.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


@Controller
public class SeckillHtmlController {
    @Autowired
    private ActivityFeignClient activityFeignClient;


    /**
     * 渲染秒杀商品列表页面
     * @param model
     * @return
     */
    @GetMapping("/seckill.html")
    public String seckillHtml(Model model){
        Result result = activityFeignClient.getSeckillGoods();
        model.addAttribute("list",result.getData());
        return "seckill/index";
    }

    /**
     * 渲染秒杀的详情页
     * @param model
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/{skuId}.html")
    public String seckillItemHtml(Model model, @PathVariable("skuId") Long skuId){
        Result result = activityFeignClient.getSecGoodsById(skuId);
        model.addAttribute("item",result.getData());
        return "seckill/item";
    }

    /**
     * 秒杀预下单，把用户的秒杀请求放在队列中（并发太大） 将用户的购买意向放入MQ，立即返回
     * 秒杀下单排队页面 （请求1 ：预下单请求发起  请求2： 定时查询状态 ）
     * @param model
     * @param skuId
     * @param buyCode
     * @return
     */
    @GetMapping("/seckill/queue.html")
    public String queueHtml(Model model, @RequestParam("skuId") Long skuId,
                            @RequestParam("skuIdStr") String buyCode){
        //不操作业务 （）
          model.addAttribute("skuId",skuId);
          model.addAttribute("skuIdStr",buyCode);
          return "/seckill/queue";
    }


    @GetMapping("/seckill/trade.html")
    public String seckillTradeHtml(Model model,@RequestParam("skuId") Long skuId){
        //调用秒杀服务获取秒杀页面数据
        Result<Map> result = activityFeignClient.seckillOrderTrade(skuId);
        model.addAllAttributes(result.getData());
        return "seckill/trade";
    }
}
