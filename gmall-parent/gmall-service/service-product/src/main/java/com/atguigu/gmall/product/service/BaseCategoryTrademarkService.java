package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseCategoryTrademark;
import com.atguigu.gmall.product.model.BaseTrademark;
import com.atguigu.gmall.product.model.CategoryTrademarkVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 分类品牌中间表 业务接口类
 *
 * @author tomcat
 * @since 2023-06-07
 */
public interface BaseCategoryTrademarkService extends IService<BaseCategoryTrademark> {
    /**
     * 查询指定分类下包含品牌列表
     *
     * @param category3Id
     * @return
     */
    List<BaseTrademark> getTrademarkList(Long category3Id);

    /**
     * 查询指定分类下未关联品牌列表（可选品牌列表）
     *
     * @param category3Id
     * @return
     */
    List<BaseTrademark> getCurrentTrademarkList(Long category3Id);

    /**
     * 将选中品牌关联到指定分类
     *
     * @param vo
     * @return
     */
    void saveBaseCategoryTrademark(CategoryTrademarkVo vo);

    /**
     * 删除指定分类下的品牌
     * {category3Id} 分类id
     * {trademarkId} 品牌id
     */
    void removeCategoryTrademark(Long category3Id, Long trademarkId);
}
