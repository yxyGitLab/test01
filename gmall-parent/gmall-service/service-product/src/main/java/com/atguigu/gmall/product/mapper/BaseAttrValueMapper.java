package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BaseAttrValueMapper extends BaseMapper<BaseAttrValue> {
}
