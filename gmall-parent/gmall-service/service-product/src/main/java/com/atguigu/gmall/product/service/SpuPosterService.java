package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SpuPoster;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品海报表 业务接口类
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuPosterService extends IService<SpuPoster> {

}
