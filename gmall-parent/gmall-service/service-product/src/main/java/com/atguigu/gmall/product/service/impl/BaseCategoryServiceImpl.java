package com.atguigu.gmall.product.service.impl;

import com.atguigu.gmall.product.mapper.BaseCategory1Mapper;
import com.atguigu.gmall.product.mapper.BaseCategory2Mapper;
import com.atguigu.gmall.product.mapper.BaseCategory3Mapper;
import com.atguigu.gmall.product.model.BaseCategory1;
import com.atguigu.gmall.product.model.BaseCategory2;
import com.atguigu.gmall.product.model.BaseCategory3;
import com.atguigu.gmall.product.service.BaseCategoryService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BaseCategoryServiceImpl implements BaseCategoryService {
    @Autowired
    private BaseCategory1Mapper baseCategory1Mapper;
    @Autowired
    private BaseCategory2Mapper baseCategory2Mapper;
    @Autowired
    private BaseCategory3Mapper baseCategory3Mapper;
    /**
     * 查询所有一级分类的列表
     * @return
     */
    @Override
    public List<BaseCategory1> getCategory1() {
        //1.调用一级分类的持久层mapper查询所有即可
        LambdaQueryWrapper<BaseCategory1> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(BaseCategory1::getId,BaseCategory1::getName);
        return baseCategory1Mapper.selectList(queryWrapper);
    }
    /**
     * 根据一级分类ID查询所属二级分类的列表
     * @param category1Id 1级分类Id
     */

    @Override
    public List<BaseCategory2> getCategory2(Long category1Id) {
        //1.构建查询条件对象QueryWrapper
        LambdaQueryWrapper<BaseCategory2> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseCategory2::getCategory1Id,category1Id);
        queryWrapper.select(BaseCategory2::getId,BaseCategory2::getName);
        //2.执行查询
        return baseCategory2Mapper.selectList(queryWrapper);
    }
    /**
     * 根据二级分类ID查询所属二级分类的列表
     * @param category2Id 2级分类Id
     */
    @Override
    public List<BaseCategory3> getCategory3(Long category2Id) {
        LambdaQueryWrapper<BaseCategory3> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseCategory3::getCategory2Id,category2Id);
        queryWrapper.select(BaseCategory3::getId,BaseCategory3::getName);
        return baseCategory3Mapper.selectList(queryWrapper);
    }
}
