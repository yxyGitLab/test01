package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

import org.springframework.stereotype.Service;

@Service
public interface BaseAttrValueService extends IService<BaseAttrValue> {
}
