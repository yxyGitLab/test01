package com.atguigu.gmall.product.redis;



import java.lang.annotation.*;

/**
 * 自定义注解：缓存+分布式锁
 * 注解上修饰注解-元注解
 * 自定义注解：缓存+分布式锁 高频率访问接口（数据更新较少），业务方法加自定义注解优化
 */
@Target({ ElementType.METHOD, ElementType.TYPE})//注解使用的地方 TYPE类上  METHOD 方法上
@Retention(RetentionPolicy.RUNTIME)
@Inherited //注解是否可以被继承
@Documented//通过Javadoc命令
public @interface GmallCache {
    /**
     * 缓存中key的前缀
     * @return
     */
    String prefix() default "data";
    /**
     * 缓存中key的后缀
     */
    String suffix() default ":info";
}
