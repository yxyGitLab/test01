package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SpuImage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品图片表 业务接口类
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuImageService extends IService<SpuImage> {

}
