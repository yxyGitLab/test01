package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.SpuPoster;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品海报表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuPosterMapper extends BaseMapper<SpuPoster> {

}
