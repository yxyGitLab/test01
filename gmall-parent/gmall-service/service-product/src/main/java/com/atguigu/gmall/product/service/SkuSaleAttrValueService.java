package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SkuSaleAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * sku销售属性值 业务接口类
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValue> {

}
