package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SpuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品表 业务接口类
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuInfoService extends IService<SpuInfo> {

}
