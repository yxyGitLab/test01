package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SkuImage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 库存单元图片表 业务接口类
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuImageService extends IService<SkuImage> {

}
