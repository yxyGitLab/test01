package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.BaseTrademark;
import com.atguigu.gmall.product.model.CategoryTrademarkVo;
import com.atguigu.gmall.product.service.BaseCategoryTrademarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/product")
public class BaseCategoryTrademarkController {

    @Autowired
    private BaseCategoryTrademarkService baseCategoryTrademarkService;

    /**
     * 查询指定分类下包含品牌列表
     *
     * @param category3Id
     * @return
     */
    @GetMapping("/baseCategoryTrademark/findTrademarkList/{category3Id}")
    public Result getTrademarkList(@PathVariable("category3Id") Long category3Id) {
        List<BaseTrademark> list = baseCategoryTrademarkService.getTrademarkList(category3Id);
        return Result.ok(list);
    }

    /**
     * 查询指定分类下未关联品牌列表（可选品牌列表）
     *
     * @param category3Id
     * @return
     */
    @GetMapping("/baseCategoryTrademark/findCurrentTrademarkList/{category3Id}")
    public Result getCurrentTrademarkList(@PathVariable("category3Id") Long category3Id) {
        List<BaseTrademark> list = baseCategoryTrademarkService.getCurrentTrademarkList(category3Id);
        return Result.ok(list);
    }

    /**
     * 将选中品牌关联到指定分类
     *
     * @param vo
     * @return
     */
    @PostMapping("/baseCategoryTrademark/save")
    public Result saveBaseCategoryTrademark(@RequestBody CategoryTrademarkVo vo) {
        baseCategoryTrademarkService.saveBaseCategoryTrademark(vo);
        return Result.ok();
    }

    /**
     * 删除指定分类下的品牌
     * {category3Id} 分类id
     * {trademarkId} 品牌id
     */
    @DeleteMapping("/baseCategoryTrademark/remove/{category3Id}/{trademarkId}")
    public Result removeCategoryTrademark(@PathVariable("category3Id") Long category3Id,
                                          @PathVariable("trademarkId") Long trademarkId
    ) {
        //没主键，不能直接调用删除，只能自定义删除
        baseCategoryTrademarkService.removeCategoryTrademark(category3Id, trademarkId);
        return Result.ok();
    }
}
