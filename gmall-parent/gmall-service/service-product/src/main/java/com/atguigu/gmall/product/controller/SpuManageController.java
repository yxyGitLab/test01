package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.BaseSaleAttr;
import com.atguigu.gmall.product.model.SpuInfo;
import com.atguigu.gmall.product.service.SpuManageService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/product")
public class SpuManageController {
    @Autowired
    private SpuManageService spuManageService;

    /**
     *  商品SPU分页查询
     * @param spuInfoPage MP分页对象
     * @param category3Id 分类id
     * @return
     */
    @GetMapping("/{page}/{size}")
    public Result getSpuByPage(@PathVariable("page") Long page,
                               @PathVariable("size") Long size,
                               @RequestParam("category3Id") Long category3Id
                               ){
        //1.构建分页对象
        Page<SpuInfo> spuInfoPage = new Page<>(page, size);
        //2.调用业务层实现分页
        spuInfoPage=  spuManageService.getSpuByPage(spuInfoPage,category3Id);
        return Result.ok(spuInfoPage);
    }
    /**
     * 查询当前平台包含所有销售属性列表
     *
     * @return
     */
    @GetMapping("/baseSaleAttrList")
    public Result getBaseSaleAttrList() {
        List<BaseSaleAttr> list = spuManageService.getBaseSaleAttrList();
        return Result.ok(list);
    }

    /**
     * 保存商品SPU信息
     *
     * @param spuInfo
     * @return
     */
    @PostMapping("/saveSpuInfo")
    public Result saveSpuInfo(@RequestBody SpuInfo spuInfo) {
        spuManageService.saveSpuInfo(spuInfo);
        return Result.ok();
    }
}
