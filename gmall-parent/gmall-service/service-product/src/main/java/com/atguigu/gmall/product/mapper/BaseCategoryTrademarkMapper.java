package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseCategoryTrademark;
import com.atguigu.gmall.product.model.BaseTrademark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分类品牌中间表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-07
 */
public interface BaseCategoryTrademarkMapper extends BaseMapper<BaseCategoryTrademark> {
    /**
     * 查询未关联到品牌集合
     * @param category3Id
     * @return
     */
    List<BaseTrademark> getCurrentTrademarkList(@Param("category3Id") Long category3Id);
}
