package com.atguigu.gmall.product.service;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.product.model.BaseCategoryView;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * VIEW 业务接口类
 * @author tomcat
 * @since 2023-06-11
 */
public interface BaseCategoryViewService extends IService<BaseCategoryView> {
    /**
     * 查询所有1级分类列表，要求将二级分类包含到一级分类下，
     * 三级分类包含到二级分类下
     * @return
     */
    List<JSONObject> getBaseCategoryList();

}
