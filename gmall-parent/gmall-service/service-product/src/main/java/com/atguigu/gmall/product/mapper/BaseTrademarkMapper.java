package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseTrademark;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 品牌表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-07
 */
public interface BaseTrademarkMapper extends BaseMapper<BaseTrademark> {

}
