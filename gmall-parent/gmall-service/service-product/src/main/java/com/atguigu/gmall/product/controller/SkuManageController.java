package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.SkuInfo;
import com.atguigu.gmall.product.model.SpuImage;
import com.atguigu.gmall.product.model.SpuSaleAttr;
import com.atguigu.gmall.product.service.SkuManageService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/product")
public class SkuManageController {

    @Autowired
    private SkuManageService skuManageService;


    /**
     * 根据spuId查询Spu包含所有商品图片列表
     * @param spuId
     * @return
     */
    @GetMapping("/spuImageList/{spuId}")
    public Result getSpuImageList(@PathVariable("spuId") Long spuId) {
        List<SpuImage> list = skuManageService.getSpuImageList(spuId);
        return Result.ok(list);
    }

    /**
     * 查询指定SPU商品包含销售属性列表(每个销售属性包含销售属性值列表)
     * @param spuId
     * @return
     */
    @GetMapping("/spuSaleAttrList/{spuId}")
    public Result getSpuSaleAttrList(@PathVariable("spuId") Long spuId) {
        List<SpuSaleAttr> list = skuManageService.getSpuSaleAttrList(spuId);
        return Result.ok(list);
    }
    /**
     * 保存商品SKU
     * @param skuInfo
     * @return
     */
    @PostMapping("/saveSkuInfo")
    public Result saveSkuInfo(@RequestBody SkuInfo skuInfo) {
        skuManageService.saveSkuInfo(skuInfo);
        return Result.ok();
    }
    /**
     * 分页查询商品SKU列表
     * @param page        页码
     * @param limit       页大小
     * @param category3Id 分类ID
     * @return
     */
    @GetMapping("/list/{page}/{limit}")
    public Result getSkuByPage(@PathVariable("page") long page,
                               @PathVariable("limit") long limit,
                               @RequestParam("category3Id") Long category3Id) {
        Page<SkuInfo> infoPage = new Page<>(page, limit);
        infoPage = skuManageService.getSkuByPage(infoPage, category3Id);
        return Result.ok(infoPage);
    }
    /**
     * 商品SKU上架
     * @param skuId
     * @return
     */
    @GetMapping("/onSale/{skuId}")
    public Result onSale(@PathVariable("skuId") Long skuId) {
        skuManageService.onSale(skuId);
        return Result.ok();
    }


    /**
     * 商品SKU下架
     * @param skuId
     * @return
     */
    @GetMapping("/cancelSale/{skuId}")
    public Result cancelSale(@PathVariable("skuId") Long skuId) {
        skuManageService.cancelSale(skuId);
        return Result.ok();
    }
}