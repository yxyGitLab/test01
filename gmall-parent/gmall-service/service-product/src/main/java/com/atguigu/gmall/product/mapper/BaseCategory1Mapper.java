package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseCategory1;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BaseCategory1Mapper extends BaseMapper<BaseCategory1> {
}
