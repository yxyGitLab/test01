package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.SkuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 库存单元表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuInfoMapper extends BaseMapper<SkuInfo> {

}
