package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.SkuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 库存单元图片表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuImageMapper extends BaseMapper<SkuImage> {

}
