package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseAttrInfo;
import com.atguigu.gmall.product.model.SkuAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sku平台属性值关联表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuAttrValueMapper extends BaseMapper<SkuAttrValue> {
    /**
     * 根据skuId查询当前SKU商品包含平台属性列表（包含属性值）
     * @return
     */
    List<BaseAttrInfo> getAttrList(@Param("skuId") Long skuId);
}
