package com.atguigu.gmall.product.service.impl;

import com.atguigu.gmall.product.model.BaseTrademark;
import com.atguigu.gmall.product.mapper.BaseTrademarkMapper;
import com.atguigu.gmall.product.service.BaseTrademarkService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 品牌表 业务实现类
 *
 * @author tomcat
 * @since 2023-06-07
 */
@Service
public class BaseTrademarkServiceImpl extends ServiceImpl<BaseTrademarkMapper, BaseTrademark> implements BaseTrademarkService {

    @Override
    public Page<BaseTrademark> getBaseTrademarkByPage(Page<BaseTrademark> ipage) {
        //1.构建分页对象，入参已有，条件满足
        //2.构建分页查询条件对象
        LambdaQueryWrapper<BaseTrademark> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(BaseTrademark::getUpdateTime);
        //3.调用当前业务层（公共service）提供分页方法
        return this.page(ipage,queryWrapper);
    }

    @Override
    public void removeCategoryTrademark(Long category3Id, Long trademarkId) {

    }
}
