package com.atguigu.gmall.product.service.impl;

import com.alibaba.nacos.client.naming.utils.CollectionUtils;
import com.atguigu.gmall.product.mapper.BaseAttrInfoMapper;
import com.atguigu.gmall.product.model.BaseAttrInfo;
import com.atguigu.gmall.product.model.BaseAttrValue;
import com.atguigu.gmall.product.service.BaseAttrInfoService;
import com.atguigu.gmall.product.service.BaseAttrValueService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BaseAttrInfoServiceImpl extends ServiceImpl<BaseAttrInfoMapper, BaseAttrInfo> implements BaseAttrInfoService {

 @Autowired
 private BaseAttrValueService baseAttrValueService;

    /**
     * 根据1，2，3级ID查询分类下关联平台属性列表（包含平台属性值）
     * @param category1Id 一级分类ID
     * @param category2Id 二级分类ID
     * @param category3Id 三级分类ID
     * @return
     */
    @Override
    public List<BaseAttrInfo> getAttrInfoList(Long category1Id, Long category2Id, Long category3Id) {
       //1.实现方式一：先根据分类ID查询平台属性列表 再遍历平台属性列表，过程中查询该平台属性包含属性值手动封装
        //2.实现方式二：编写自定义的SQL 一对多的配置完成查询
        List<BaseAttrInfo> list = this.baseMapper.getAttrInfoList(category1Id, category2Id, category3Id);
        return list;
    }
    /**
     * 新增/修改平台属性
     * @param baseAttrInfo 平台属性信息
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdateAttrInfo(BaseAttrInfo baseAttrInfo) {
        //1.判断提交参数中有id,修改操作，无Id,新增操作
        if (baseAttrInfo.getId() != null) {
            //修改 平台属性名称 平台属性也会修改
            //修改平台属性信息
            this.updateById(baseAttrInfo);
            //先将旧的平台值删除，再新增平台属性
            LambdaQueryWrapper<BaseAttrValue> queryWrapper=new LambdaQueryWrapper<>();
            queryWrapper.eq(BaseAttrValue::getAttrId,baseAttrInfo.getId());
            baseAttrValueService.remove(queryWrapper);
            //批量的新增平台属性值 ctrl+alt+m 抽取方法
            saveBaseAttrInfo(baseAttrInfo);

        } else {
            //新增
            //将提交的平台属性信息执行保存，保存后得到平台属性ID
            this.save(baseAttrInfo);
            //获取到平台属性值集合，批量进行保存
            saveBaseAttrInfo(baseAttrInfo);
        }
    }

    /**
     * 批量新增平台属性值方法
     * @param baseAttrInfo
     */
    private void saveBaseAttrInfo(BaseAttrInfo baseAttrInfo) {
        List<BaseAttrValue> attrValueList = baseAttrInfo.getAttrValueList();
        if (!CollectionUtils.isEmpty(attrValueList)) {
            attrValueList.stream().forEach(baseAttrValue -> {
                baseAttrValue.setAttrId(baseAttrInfo.getId());
            });
            baseAttrValueService.saveBatch(attrValueList);
        }
    }

    /**
     * 根据平台属性ID查询该属性包含属性值列表
     * @param attrId
     * @return
     */
    @Override
    public List<BaseAttrValue> getAttrValueList(Long attrId) {
        LambdaQueryWrapper<BaseAttrValue> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseAttrValue::getAttrId, attrId);
        return baseAttrValueService.list(queryWrapper);
    }
}
