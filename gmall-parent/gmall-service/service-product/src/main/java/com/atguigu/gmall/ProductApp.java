package com.atguigu.gmall;

import com.atguigu.gmall.common.constant.RedisConst;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication
@EnableDiscoveryClient
public class ProductApp implements CommandLineRunner {

    @Autowired
    private RedissonClient redissonClient;
    public static void main(String[] args) {
        SpringApplication.run(ProductApp.class, args);
    }

    /**
     * 初始化布隆过滤器
     * springboot项目启动成功后，执行依次该方法
     * @param args incoming main method arguments
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        RBloomFilter<Long> bloomFilter = redissonClient.getBloomFilter(RedisConst.SKU_BLOOM_FILTER);
        //2.创建设置布隆过滤器数据规模，误判率  商品数量50万 误判率0.03%
        bloomFilter.tryInit(500000,0.03);
    }
}