package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.SpuSaleAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * spu销售属性值 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuSaleAttrValueMapper extends BaseMapper<SpuSaleAttrValue> {

}
