package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.BaseAttrInfo;
import com.atguigu.gmall.product.model.BaseAttrValue;
import com.atguigu.gmall.product.service.BaseAttrInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/admin/product")
public class BaseAttrInfoController {
    @Autowired
    private BaseAttrInfoService baseAttrInfoService;

    /**
     * 根据1，2，3级ID查询分类下关联平台属性列表（包含平台属性值）
     * @param category1Id 一级分类ID
     * @param category2Id 二级分类ID
     * @param category3Id 三级分类ID
     * @return
     */
    @GetMapping("/attrInfoList/{category1Id}/{category2Id}/{category3Id}")
    public Result getAttrInfoList(
            @PathVariable("category1Id") Long category1Id,
            @PathVariable("category2Id") Long category2Id,
            @PathVariable("category3Id") Long category3Id
    ){
        List<BaseAttrInfo> list=baseAttrInfoService.getAttrInfoList(category1Id,category2Id,category3Id);
        return Result.ok(list);
    }

    /**
     * 新增/修改平台属性
     * @param baseAttrInfo 平台属性信息
     * @return
     */
    @PostMapping("/saveAttrInfo")
    public Result saveOrUpdateAttrInfo(@RequestBody BaseAttrInfo baseAttrInfo) {
        //一般增删改业务方法返回void即可,未发生异常调用成功
        baseAttrInfoService.saveOrUpdateAttrInfo(baseAttrInfo);
        return Result.ok();
    }

    /**
     * 根据平台属性ID查询该属性包含属性值列表
     * @param attrId
     * @return
     */
    @GetMapping("/getAttrValueList/{attrId}")
    public Result getAttrValueList(@PathVariable("attrId") Long attrId){
        List<BaseAttrValue> list = baseAttrInfoService.getAttrValueList(attrId);
        return Result.ok(list);
    }
}
