package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseAttrInfo;
import com.atguigu.gmall.product.model.BaseAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface BaseAttrInfoService extends IService<BaseAttrInfo>{
    /**
     * 根据1，2，3级ID查询分类下关联平台属性列表（包含平台属性值）
     * @param category1Id 一级分类ID
     * @param category2Id 二级分类ID
     * @param category3Id 三级分类ID
     * @return
     */
    List<BaseAttrInfo> getAttrInfoList(Long category1Id, Long category2Id, Long category3Id);

    /**
     * 新增/修改平台属性
     * @param baseAttrInfo 平台属性信息
     * @return
     */
    void saveOrUpdateAttrInfo(BaseAttrInfo baseAttrInfo);
    /**
     * 根据平台属性ID查询该属性包含属性值列表
     * @param attrId
     * @return
     */
    List<BaseAttrValue> getAttrValueList(Long attrId);
}
