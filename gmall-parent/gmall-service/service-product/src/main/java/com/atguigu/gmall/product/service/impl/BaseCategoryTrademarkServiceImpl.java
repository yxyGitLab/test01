package com.atguigu.gmall.product.service.impl;

import com.alibaba.nacos.client.naming.utils.CollectionUtils;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.BaseCategoryTrademark;
import com.atguigu.gmall.product.mapper.BaseCategoryTrademarkMapper;
import com.atguigu.gmall.product.model.BaseTrademark;
import com.atguigu.gmall.product.model.CategoryTrademarkVo;
import com.atguigu.gmall.product.service.BaseCategoryTrademarkService;
import com.atguigu.gmall.product.service.BaseTrademarkService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 分类品牌中间表 业务实现类
 *
 * @author tomcat
 * @since 2023-06-07
 */
@Service
public class BaseCategoryTrademarkServiceImpl extends ServiceImpl<BaseCategoryTrademarkMapper, BaseCategoryTrademark> implements BaseCategoryTrademarkService {

    @Autowired
    private BaseTrademarkService baseTrademarkService;

    /**
     * 查询指定分类下包含品牌列表
     *
     * @param category3Id
     * @return
     */
    @Override
    public List<BaseTrademark> getTrademarkList(Long category3Id) {
        //1.根据分类ID查询 分类品牌 关系表中，当前分类下包含品牌id集合
        LambdaQueryWrapper<BaseCategoryTrademark> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseCategoryTrademark::getCategory3Id, category3Id);
        List<BaseCategoryTrademark> categoryTrademarkList = this.list(queryWrapper);
        if (!CollectionUtils.isEmpty(categoryTrademarkList)) {
            //如果集合有值，遍历集合获取集合中每个对象品牌ID
            //Stream流代码简洁，在多核cpu中可以发挥CPU的性能
            //遍历集合获取集合对象中每个对象品牌ID
            List<Long> tradeMarkIdList = categoryTrademarkList.stream().map(baseCategoryTrademark -> {
                return baseCategoryTrademark.getTrademarkId();
            }).collect(Collectors.toList());

            //2.根据品牌ID集合查询品牌集合
            return baseTrademarkService.listByIds(tradeMarkIdList);
        }
        return null;
    }

    /**
     * 查询指定分类下未关联品牌列表（可选品牌列表）
     *
     * @param category3Id
     * @return
     */
    @Override
    public List<BaseTrademark> getCurrentTrademarkList(Long category3Id) {
        //1.获取持久层mapper,调用mapper自定义方法，编写动态SQL
        return this.getBaseMapper().getCurrentTrademarkList(category3Id);
    }

    /**
     * 将选中品牌关联到指定分类
     *
     * @param vo
     * @return
     */
    @Override
    public void saveBaseCategoryTrademark(CategoryTrademarkVo vo) {
        //1.从Vo对象中得到品牌ID集合 将该集合类型转为 分类品牌关系对象
        List<Long> trademarkIdList = vo.getTrademarkIdList();
        if (!CollectionUtils.isEmpty(trademarkIdList)) {
            List<BaseCategoryTrademark> categoryTrademarkList = trademarkIdList.stream().map(tradeMarkId -> {
                //将集合泛型从Long品牌ID 转为 分类品牌关系对象 给对象中属性赋值
                BaseCategoryTrademark baseCategoryTrademark = new BaseCategoryTrademark();
                baseCategoryTrademark.setTrademarkId(tradeMarkId);
                baseCategoryTrademark.setCategory3Id(vo.getCategory3Id());
                return baseCategoryTrademark;
            }).collect(Collectors.toList());
            //2.批量保存分类品牌关系
            this.saveBatch(categoryTrademarkList);
        }
    }

    /**
     * 删除指定分类下的品牌
     * {category3Id} 分类id
     * {trademarkId} 品牌id
     */
    @Override
    public void removeCategoryTrademark(Long category3Id, Long trademarkId) {
        LambdaQueryWrapper<BaseCategoryTrademark> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BaseCategoryTrademark::getCategory3Id, category3Id);
        queryWrapper.eq(BaseCategoryTrademark::getTrademarkId, trademarkId);
        this.remove(queryWrapper);
    }
}
