package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.BaseCategory1;
import com.atguigu.gmall.product.model.BaseCategory2;
import com.atguigu.gmall.product.model.BaseCategory3;
import com.atguigu.gmall.product.service.BaseCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/product")
public class BaseCategoryController {

    @Autowired
    private BaseCategoryService baseCategoryService;

    /**
     * 查询所有一级分类的列表
     * @return
     */
    @GetMapping("/getCategory1")
    public Result getCategory1(){
     List<BaseCategory1> list= baseCategoryService.getCategory1();
     return Result.ok(list);
    }
    /**
    * 根据一级分类ID查询所属二级分类的列表
     * @param category1Id 1级分类Id
    */
    @GetMapping("/getCategory2/{category1Id}")
    public Result getCategory2(@PathVariable("category1Id") Long category1Id){
        List<BaseCategory2> list=baseCategoryService.getCategory2(category1Id);
        return Result.ok(list);
    }
    /**
     * 根据二级分类ID查询所属二级分类的列表
     * @param category2Id 2级分类Id
     */
    @GetMapping("/getCategory3/{category2Id}")
    public Result getCategory3(@PathVariable("category2Id") Long category2Id){
        List<BaseCategory3> list=baseCategoryService.getCategory3(category2Id);
        return Result.ok(list);
    }

}
