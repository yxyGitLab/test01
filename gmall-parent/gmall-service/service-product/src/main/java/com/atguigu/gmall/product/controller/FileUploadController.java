package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/admin/product")
public class FileUploadController {


    @Autowired
    private FileUploadService fileUploadService;

    /**
     * 文件上传
     *
     * @param file
     * @return 上传后在线图片地址
     */
    @PostMapping("/fileUpload")
    public Result fileUpload(@RequestParam MultipartFile file) {
        String fileUrl = fileUploadService.fileUpload(file);
        return Result.ok(fileUrl);
    }
}