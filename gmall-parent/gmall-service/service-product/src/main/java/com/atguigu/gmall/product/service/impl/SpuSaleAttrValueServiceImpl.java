package com.atguigu.gmall.product.service.impl;

import com.atguigu.gmall.product.model.SpuSaleAttrValue;
import com.atguigu.gmall.product.mapper.SpuSaleAttrValueMapper;
import com.atguigu.gmall.product.service.SpuSaleAttrValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * spu销售属性值 业务实现类
 *
 * @author tomcat
 * @since 2023-06-08
 */
@Service
public class SpuSaleAttrValueServiceImpl extends ServiceImpl<SpuSaleAttrValueMapper, SpuSaleAttrValue> implements SpuSaleAttrValueService {
}
