package com.atguigu.gmall.product.redis;

import com.alibaba.nacos.client.naming.utils.CollectionUtils;
import com.atguigu.gmall.common.constant.RedisConst;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 自定义注解的切面类，对使用注解@GmallCache注解方法进行增强
 */
@Aspect//标识为切面类
@Component
@Slf4j
public class GmallCacheAspect {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 环绕通知，对使用注解@GmallCache方法所在类产生代理对象，增强方法逻辑功能
     *
     * @param pjp 切入点，增强的功能
     * @return
     */
    @Around("@annotation(com.atguigu.gmall.product.redis.GmallCache)")
    public Object cacheAndLockAspect(ProceedingJoinPoint pjp) {
        try {
            //0.定义返回结果-封装各种业务数据
            Object objectResult = new Object();
            //一.前置通知
            log.info("前置通知");
            //1.优先从分布式缓存redis获取业务数据
            //1.1组装业务数据的key 问题：怎么获取注解中属性？  反射方式获取,从切入点pjp入手
            //1.1.1获取切入点方法的签名
            MethodSignature signature = (MethodSignature) pjp.getSignature();
            //1.1.2获取方法信息对象
            Method method = signature.getMethod();
            //1.1.3获取方法上的注解
            GmallCache annotation = method.getAnnotation(GmallCache.class);
            //1.1.4获取注解内部的属性
            //获取前缀
            String prefix = annotation.prefix();
            //获取后缀
            String suffix = annotation.suffix();
            //1.1.5获取方法的参数 业务Key中间标识
            String methodArgs = "none";
            //1.1.6获取方法参数 将参数值获取到
            List<Object> objects = Arrays.asList(pjp.getArgs());
            if (!CollectionUtils.isEmpty(objects)) {
                methodArgs = objects.stream().map(arg -> arg.toString()).collect(Collectors.joining(":"));
            }
            String dataKey = prefix + methodArgs + suffix;
            //1.2从redis中获取缓存业务数据
            objectResult = redisTemplate.opsForValue().get(dataKey);

            if (objectResult != null) {
                //2.命中缓存则直接响应业务数据
                return objectResult;
            } else {
                //3.未命中缓存则调用尝试获取分布式锁
                //3.1拼接锁的key 减少锁的力度，为不同查询业务参数设置各自锁
                String lockKey = prefix + methodArgs + RedisConst.SKULOCK_SUFFIX;
                //3.2创建锁对象
                RLock lock = redissonClient.getLock(lockKey);
                //3.3获取分布式锁
                boolean flag = lock.tryLock(RedisConst.SKULOCK_EXPIRE_PX2, RedisConst.SKULOCK_EXPIRE_PX1, TimeUnit.SECONDS);
                if (flag) {
                    try {
                        //二.查询数据库操作，执行目标方法
                        objectResult = pjp.proceed();
                        //三.后置通知
                        log.info("后置通知");
                        //4.将查询数据库结果放入缓存
                        if (objectResult != null) {
                            //查询数据库有结果，将结果放入到缓存,业务数据基础时间24小时+随机值（解决缓存雪崩）
                            int randNum=new Random().nextInt(600);
                            Long time = RedisConst.SKUKEY_TIMEOUT + randNum;
                            redisTemplate.opsForValue().set(dataKey, objectResult, time, TimeUnit.SECONDS);
                            return objectResult;
                        } else {
                            //查询数据库没有结果，也得放入缓存，缓存过期时间5分钟
                            redisTemplate.opsForValue().set(dataKey, objectResult, 5, TimeUnit.MINUTES);
                            return objectResult;
                        }
                    } finally {
                        //5.释放锁
                        lock.unlock();
                    }
                } else {
                    //获取锁失败 ，需要自旋（保证用户请求线程一定能获取到业务数据）
                    TimeUnit.MILLISECONDS.sleep(500);
                    this.cacheAndLockAspect(pjp);
                }
            }
            return objectResult;
        } catch (Throwable e) {
            try {
                //当锁服务不可用，兜底处理方案查询数据库
                return pjp.proceed();
            } catch (Throwable ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
