package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SpuSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * spu销售属性 业务接口类
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuSaleAttrService extends IService<SpuSaleAttr> {

}
