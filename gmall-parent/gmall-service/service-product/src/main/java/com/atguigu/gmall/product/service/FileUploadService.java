package com.atguigu.gmall.product.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
    /**
     * 文件上传
     *
     * @param file
     * @return 上传后在线图片地址
     */
    String fileUpload(MultipartFile file);
}