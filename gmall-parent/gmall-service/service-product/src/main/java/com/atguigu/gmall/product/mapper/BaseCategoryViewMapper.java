package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseCategoryView;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * VIEW Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-11
 */
public interface BaseCategoryViewMapper extends BaseMapper<BaseCategoryView> {

}
