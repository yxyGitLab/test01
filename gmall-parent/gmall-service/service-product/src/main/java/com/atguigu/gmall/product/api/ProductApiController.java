package com.atguigu.gmall.product.api;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.*;
import com.atguigu.gmall.product.service.BaseCategoryViewService;
import com.atguigu.gmall.product.service.BaseTrademarkService;
import com.atguigu.gmall.product.service.SkuManageService;
import com.atguigu.gmall.product.service.SpuManageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 用于服务之间调用，远程调用的都是以api开头
 */
@Slf4j
@RestController
@RequestMapping("/api/product")
public class ProductApiController {
    @Autowired
    private SkuManageService skuManageService;

    @Autowired
    private SpuManageService spuManageService;

    @Autowired
    private BaseTrademarkService baseTrademarkService;

    /**
     * 注入分类视图
     * @param skuId
     * @return
     */
    @Autowired
    private BaseCategoryViewService baseCategoryViewService;

    /**
     * 根据SkuId查询商品信息-包含图片列表
     * 如果不想再用Result.data取数据，可以直接使用SkuInfo返回信息
     * 仅适用于两个服务间进行调用，但是给前端返回数据需要使用Result
     * @param skuId
     * @return
     */
    @GetMapping("/inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable("skuId") Long skuId, @RequestHeader(required = false,value = "testheader") String testheader){
        log.info("收到头信息"+testheader);
        return skuManageService.getSkuInfoFromDB(skuId);
    }

    /**
     * 根据三级分类Id获取分类信息
     * @param category3Id
     * @return
     */
    @GetMapping("/inner/getCategoryView/{category3Id}")
    public BaseCategoryView getCategoryView(@PathVariable("category3Id") Long category3Id){
        return skuManageService.getCategoryView(category3Id);
    }
    /**
     * 根据SKuId查询商品最新价格
     */
    @GetMapping("/inner/getSkuPrice/{skuId}")
    public BigDecimal getSkuPrice(@PathVariable("skuId") Long skuId){
        return skuManageService.getSkuPrice(skuId);
    }

    /**
     * 根据商品SpuId查询海报图片列表
     * @param spuId
     * @return
     */
    @GetMapping("/inner/findSpuPosterBySpuId/{spuId}")
    public List<SpuPoster> getSpuPosterBySpuId(@PathVariable("spuId") Long spuId){
        return spuManageService.getSpuPosterBySpuId(spuId);
    }

    /**
     * 根据skuId查询当前SKU商品包含平台属性列表（包含属性值）
     * @return
     */
     @GetMapping("/inner/getAttrList/{skuId}")
    public List<BaseAttrInfo> getAttrList(@PathVariable("skuId") Long skuId){
         return skuManageService.getAttrList(skuId);
     }

    /**
     *  查询spu商品所有销售属性，查询指定sku销售属性选中效果
     * @param skuId
     * @param spuId
     * @return
     */
     @GetMapping("/inner/getSpuSaleAttrListCheckBySku/{skuId}/{spuId}")
    public List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(@PathVariable("skuId") Long skuId,
                                                          @PathVariable("spuId") Long spuId){
         return skuManageService.getSpuSaleAttrListCheckBySku(skuId, spuId);
     }
    /**
     * 在一组SPU商品下 切换不同SKU字符串
     * @param spuId "{'销售属性1|销售属性2':'sku商品ID','销售属性1|销售属性3':'sku商品ID'}"
     * @return
     */
    @GetMapping("/inner/getSkuValueIdsMap/{spuId}")
    public String getSkuValueIdsMap(@PathVariable("spuId") Long spuId){
        return skuManageService.getSkuValueIdsMap(spuId);
    }

    /**
     * 查询所有1级分类列表，要求将二级分类包含到一级分类下，
     * 三级分类包含到二级分类下
     * @return
     */
    @GetMapping("/inner/getBaseCategoryList")
    public List<JSONObject> getBaseCategoryList(){
        return baseCategoryViewService.getBaseCategoryList();
    }

    /**
     *根据品牌的id查询品牌的信息
     * @param tmId
     * @return
     */
    @GetMapping("/inner/getTrademark/{tmId}")
    public BaseTrademark getBaseTrademarkById(@PathVariable("tmId") Long tmId){
        return baseTrademarkService.getById(tmId);
    }
}
