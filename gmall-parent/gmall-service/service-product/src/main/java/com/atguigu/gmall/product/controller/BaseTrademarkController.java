package com.atguigu.gmall.product.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.product.model.BaseTrademark;
import com.atguigu.gmall.product.service.BaseTrademarkService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/product")
public class BaseTrademarkController {

    @Autowired
    private BaseTrademarkService baseTrademarkService;

    /**
     * 品牌分页
     * 单表操作
     * @param ipage 分页对象
     */
    @GetMapping("/baseTrademark/{page}/{limit}")
    public Result getBaseTrademarkByPage(
            @PathVariable("page") Long page,
            @PathVariable("limit") Long limit) {
        //1.构建业务层分页查询需要page对象，web层封装分页参数中当前页码，页大小
        Page<BaseTrademark> ipage = new Page<>(page, limit);
        /*2.调用业务层实现条件分页查询，其它分页属性，总记录数，总页数，当前页记录在
         * 业务层中调用持久层查询后才可以封装
         * */
        ipage = baseTrademarkService.getBaseTrademarkByPage(ipage);
        return Result.ok(ipage);
    }

    /**
     * 前端请求发过来是json数据，需用@RequesBody注解进行接收
     * 保存品牌  单表操作
     * @param baseTrademark 对象接收前端传过来的数据
     * @return
     */
    @PostMapping("/baseTrademark/save")
    public Result saveBaseTrademark(@RequestBody BaseTrademark baseTrademark){
        //继承的父类中有save方法，可以在这使用
        baseTrademarkService.save(baseTrademark);
        return Result.ok();
    }

    /**
     * 根据品牌ID 查询品牌ID
     * @param id 单表操作
     * @return
     */
    @GetMapping("/baseTrademark/get/{id}")
    public Result getBaseTrademarkById(@PathVariable("id") Long id){
        BaseTrademark baseTrademark = baseTrademarkService.getById(id);
        return Result.ok(baseTrademark);
    }

    /**
     * 修改品牌信息
     * 单表操作
     * @param baseTrademark
     * @return
     */
    @PutMapping("/baseTrademark/update")
    public Result updateBaseTrademark(@RequestBody BaseTrademark baseTrademark){
        baseTrademarkService.updateById(baseTrademark);
        return Result.ok();
    }
    /**
     * 删除品牌
     * @param id
     * @return
     */
    @DeleteMapping("/baseTrademark/remove/{id}")
    public Result deleteBaseTrademark(@PathVariable("id") Long id){
        baseTrademarkService.removeById(id);
        return Result.ok();
    }
}
