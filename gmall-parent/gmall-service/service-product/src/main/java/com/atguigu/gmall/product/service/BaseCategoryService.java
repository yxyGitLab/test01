package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseCategory1;
import com.atguigu.gmall.product.model.BaseCategory2;
import com.atguigu.gmall.product.model.BaseCategory3;

import java.util.List;

public interface BaseCategoryService {
    /**
     * 查询所有一级分类的列表
     * @return
     */
    List<BaseCategory1> getCategory1();
    /**
     * 根据一级分类ID查询所属二级分类的列表
     * @param category1Id 1级分类Id
     */
    List<BaseCategory2> getCategory2(Long category1Id);

    /**
     * 根据二级分类ID查询所属二级分类的列表
     * @param category2Id 2级分类Id
     */
    List<BaseCategory3> getCategory3(Long category2Id);
}
