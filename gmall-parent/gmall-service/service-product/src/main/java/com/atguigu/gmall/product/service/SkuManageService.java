package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.math.BigDecimal;
import java.util.List;
//ctrl+alt+B进入实现类
public interface SkuManageService {

    /**
     * 从数据库进行查询
     * @param skuId
     * @return
     */
    SkuInfo getSkuInfoFromDB(Long skuId);
    /**
     * 根据spuId查询Spu包含所有商品图片列表
     * @param spuId
     * @return
     */
    List<SpuImage> getSpuImageList(Long spuId);
    /**
     * 查询指定SPU商品包含销售属性列表(每个销售属性包含销售属性值列表)
     * @param spuId
     * @return
     */
    List<SpuSaleAttr> getSpuSaleAttrList(Long spuId);
    /**
     * 保存商品SKU
     * @param skuInfo
     * @return
     */
    void saveSkuInfo(SkuInfo skuInfo);
    /**
     * 分页查询商品SKU列表
     * @param   infoPage   分页对象
     * @param category3Id 分类ID
     * @return
     */
    Page<SkuInfo> getSkuByPage(Page<SkuInfo> infoPage, Long category3Id);
    /**
     * 商品SKU上架
     * @param skuId
     * @return
     */
    void onSale(Long skuId);
    /**
     * 商品SKU下架
     * @param skuId
     * @return
     */
    void cancelSale(Long skuId);
    /**
     * 根据SkuId查询商品信息-包含图片列表
     * 如果不想再用Result.data取数据，可以直接使用SkuInfo返回信息
     * 仅适用于两个服务间进行调用，但是给前端返回数据需要使用Result
     * @param skuId
     * @return
     */
    SkuInfo getSkuInfo(Long skuId);
    /**
     * 根据三级分类Id获取分类信息
     * @param category3Id
     * @return
     */
    BaseCategoryView getCategoryView(Long category3Id);
    /**
     * 根据SKuId查询商品最新价格
     */
    BigDecimal getSkuPrice(Long skuId);
    /**
     * 根据skuId查询当前SKU商品包含平台属性列表（包含属性值）
     * @return
     */
    List<BaseAttrInfo> getAttrList(Long skuId);
    /**
     *  查询spu商品所有销售属性，查询指定sku销售属性选中效果
     * @param skuId
     * @param spuId
     * @return
     */
    List<SpuSaleAttr> getSpuSaleAttrListCheckBySku(Long skuId, Long spuId);
    /**
     * 在一组SPU商品下 切换不同SKU字符串
     * @param spuId "{'销售属性1|销售属性2':'sku商品ID','销售属性1|销售属性3':'sku商品ID'}"
     * @return
     */
    String getSkuValueIdsMap(Long spuId);
}
