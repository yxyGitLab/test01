package com.atguigu.gmall.product.service.impl;

import com.alibaba.nacos.common.utils.CollectionUtils;
import com.atguigu.gmall.product.model.*;
import com.atguigu.gmall.product.redis.GmallCache;
import com.atguigu.gmall.product.service.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商品SPU抽取出来的service
 */
@Service
public class SpuManageServiceImpl implements SpuManageService {
    @Autowired
    private SpuInfoService spuInfoService;
    @Autowired
    private SpuImageService spuImageService;
    @Autowired
    private SpuPosterService spuPosterService;
    @Autowired
    private SpuSaleAttrService spuSaleAttrService;
    @Autowired
    private SpuSaleAttrValueService spuSaleAttrValueService;
    @Autowired
    private BaseSaleAttrService baseSaleAttrService;
    /**
     *  商品SPU分页查询
     * @param spuInfoPage MP分页对象
     * @param category3Id 分类id
     * @return
     */
    @Override
    public Page<SpuInfo> getSpuByPage(Page<SpuInfo> spuInfoPage, Long category3Id) {
        //1.构建分页对象，入参已提供
        //2.构建查询条件
        LambdaQueryWrapper<SpuInfo> queryWrapper = new LambdaQueryWrapper<>();
        if (category3Id != null) {
            queryWrapper.eq(SpuInfo::getCategory3Id, category3Id);
        }
        queryWrapper.orderByDesc(SpuInfo::getUpdateTime);
        //3.执行分页查询
        return spuInfoService.page(spuInfoPage, queryWrapper);
    }
    /**
     * 查询当前平台包含所有销售属性列表
     *
     * @return
     */
    @Override
    public List<BaseSaleAttr> getBaseSaleAttrList() {
        return baseSaleAttrService.list();
    }

        /**
         * 保存商品SPU信息
         * 1.将提交商品SPU基本信息封装为SpuInfo对象,保存到spu_info表
         * 2.将提交多张商品图片封装图片集合对象List<SpuImage> 将图片批量保存
         * 3.将提交多张海报图片封装图片集合对象List<SpuPoster> 将海报图片批量保存
         * 4.将提交多个销售属性封装集合对象List<SpuSaleAttr> 将销售属性批量保存
         * 5.将提交每个销售属性包含属性值 封装集合对象List<SpuSaleAttrValue> 将销售属性值批量保存
         *
         * @param spuInfo
         * @return
         */
        @Override
        @Transactional(rollbackFor = Exception.class)
        public void saveSpuInfo(SpuInfo spuInfo) {
            //1.将提交商品SPU基本信息封装为SpuInfo对象,保存到spu_info表
            spuInfoService.save(spuInfo);
            Long spuId = spuInfo.getId();
            //2.将提交多张商品图片封装图片集合对象List<SpuImage> 将图片批量保存
            List<SpuImage> spuImageList = spuInfo.getSpuImageList();
            if (!CollectionUtils.isEmpty(spuImageList)) {
                spuImageList.stream().forEach(spuImage -> {
                    //2.1 将图片关联到商品SPU
                    spuImage.setSpuId(spuId);
                });
                //2.2 批量保存
                spuImageService.saveBatch(spuImageList);
            }
            //3.将提交多张海报图片封装图片集合对象List<SpuPoster> 将海报图片批量保存
            List<SpuPoster> spuPosterList = spuInfo.getSpuPosterList();
            if (!CollectionUtils.isEmpty(spuPosterList)) {
                spuPosterList.stream().forEach(spuPoster -> {
                    spuPoster.setSpuId(spuId);
                });
                spuPosterService.saveBatch(spuPosterList);
            }
            //4.将提交多个销售属性封装集合对象List<SpuSaleAttr> 将销售属性批量保存
            List<SpuSaleAttr> spuSaleAttrList = spuInfo.getSpuSaleAttrList();
            if (!CollectionUtils.isEmpty(spuSaleAttrList)) {
                spuSaleAttrList.forEach(spuSaleAttr -> {
                    //销售属性关联spu商品
                    spuSaleAttr.setSpuId(spuId);
                    //5.将提交每个销售属性包含属性值 封装集合对象List<SpuSaleAttrValue> 将销售属性值批量保存
                    List<SpuSaleAttrValue> spuSaleAttrValueList = spuSaleAttr.getSpuSaleAttrValueList();
                    if (!CollectionUtils.isEmpty(spuSaleAttrValueList)) {
                        spuSaleAttrValueList.stream().forEach(spuSaleAttrValue -> {
                            //销售属性值关联商品SPU
                            spuSaleAttrValue.setSpuId(spuId);
                            //设置销售属性名称
                            spuSaleAttrValue.setSaleAttrName(spuSaleAttr.getSaleAttrName());
                        });
                        //批量保存销售属性值
                        spuSaleAttrValueService.saveBatch(spuSaleAttrValueList);
                    }
                });
                //批量保存
                spuSaleAttrService.saveBatch(spuSaleAttrList);
            }
    }
    /**
     * 根据商品SpuId查询海报图片列表
     * @param spuId
     * @return
     */
    @Override
    @GmallCache(prefix = "spuPoster:")
    public List<SpuPoster> getSpuPosterBySpuId(Long spuId) {
        LambdaQueryWrapper<SpuPoster> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SpuPoster::getSpuId,spuId);
        return spuPosterService.list(queryWrapper);
    }
}
