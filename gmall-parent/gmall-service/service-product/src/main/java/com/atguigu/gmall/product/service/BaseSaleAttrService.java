package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseSaleAttr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 基本销售属性表 业务接口类
 * @author tomcat
 * @since 2023-06-08
 */
public interface BaseSaleAttrService extends IService<BaseSaleAttr> {

}
