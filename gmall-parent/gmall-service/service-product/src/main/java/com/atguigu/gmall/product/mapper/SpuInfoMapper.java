package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.SpuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-08
 */
public interface SpuInfoMapper extends BaseMapper<SpuInfo> {

}
