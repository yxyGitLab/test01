package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SkuAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * sku平台属性值关联表 业务接口类
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuAttrValueService extends IService<SkuAttrValue> {

}
