package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseCategory2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BaseCategory2Mapper extends BaseMapper<BaseCategory2> {
}
