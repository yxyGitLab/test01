package com.atguigu.gmall.product.mapper;

import com.atguigu.gmall.product.model.BaseCategory3;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface BaseCategory3Mapper extends BaseMapper<BaseCategory3> {
}
