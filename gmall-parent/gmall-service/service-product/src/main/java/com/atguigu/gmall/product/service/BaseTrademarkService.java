package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseTrademark;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 品牌表 业务接口类
 * @author tomcat
 * @since 2023-06-07
 */
public interface BaseTrademarkService extends IService<BaseTrademark> {
    /**
     * 品牌分页
     */
    Page<BaseTrademark> getBaseTrademarkByPage(Page<BaseTrademark> ipage);
    /**
     * 删除指定分类下的品牌
     * {category3Id} 分类id
     * {trademarkId} 品牌id
     * @return
     */
    void removeCategoryTrademark(Long category3Id, Long trademarkId);
}
