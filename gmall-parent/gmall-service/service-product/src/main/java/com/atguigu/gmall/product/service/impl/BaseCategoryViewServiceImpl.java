package com.atguigu.gmall.product.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.product.model.BaseCategoryView;
import com.atguigu.gmall.product.mapper.BaseCategoryViewMapper;
import com.atguigu.gmall.product.redis.GmallCache;
import com.atguigu.gmall.product.service.BaseCategoryViewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * VIEW 业务实现类
 *
 * @author tomcat
 * @since 2023-06-11
 */
@Service
public class BaseCategoryViewServiceImpl extends ServiceImpl<BaseCategoryViewMapper, BaseCategoryView> implements BaseCategoryViewService {
    /**
     * 查询所有1级分类列表，要求将二级分类包含到一级分类下，
     * 三级分类包含到二级分类下
     * @return
     */
    @Override
    @GmallCache(prefix = "getBaseCategoryList:")
    public List<JSONObject> getBaseCategoryList() {
        List<JSONObject> resultList=new ArrayList<>();
        //1.查询分类的视图得到所有分类的集合 共计910条
        List<BaseCategoryView> allBaseCategoryViewList = this.list();
        Map<Long, List<BaseCategoryView>> category1MapList = allBaseCategoryViewList.stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory1Id));
        //2.对所有分类集合进行分组-按照1级分类ID（category1_id）分组，将所有的一级分类放在一起
        //map中的key是一级分类ID的值，value是某个一级分类的集合
        //因为前端是从1开始的,加index这个索引值
        int index=0;
        for (Map.Entry<Long, List<BaseCategoryView>> category1MapEntry : category1MapList.entrySet()) {
            //category1MapEntry对象是
            //3.对分组后所有一级分类进行遍历，构建一级分类JsonObject对象
            //3.1获取一级分类ID
            Long category1Id = category1MapEntry.getKey();
            //3.2获取一级分类名称,只需从集合中第一条记录，获取分类名称
            //得到第一条字段
            String category1Name = category1MapEntry.getValue().get(0).getCategory1Name();
            //3.3构建一级分类的JsonObject对象
            JSONObject category1 = new JSONObject();
            //变量名必须叫 categoryId,因为前端叫这个名字
            category1.put("categoryId",category1Id);
            //第二个属性名 必须叫 categoryName 前端叫这个名字
            category1.put("categoryName",category1Name);
            //第三个属性名 前端索引index
            category1.put("index",index++);
            //每循环依次往里面装一个一级分类的对象
            resultList.add(category1);
            //4.在上面某个一级分类中，再次对当前一级分类集合按照2级分类id(category2_id)分组
            Map<Long, List<BaseCategoryView>> category2MapList = category1MapEntry.getValue().stream().collect(Collectors.groupingBy(BaseCategoryView::getCategory2Id));

            List<JSONObject> category2List=new ArrayList<>();
            //5.在对分组后所有二级分类进行遍历，构建二级分类JsonObject对象 ，把二级分类封装到集合中，给1级分类的子节点分类属性赋值
            for (Map.Entry<Long, List<BaseCategoryView>> category2MapEntry : category2MapList.entrySet()) {
                //5.1获取二级分类ID
                Long category2Id = category2MapEntry.getKey();
                //5.2获取二级分类名称
                String category2Name = category2MapEntry.getValue().get(0).getCategory2Name();
                JSONObject category2 = new JSONObject();
                category2.put("categoryId",category2Id);
                category2.put("categoryName",category2Name);
                category2List.add(category2);
                //6.同时得到三级分类，封装到集合中，给二级分类子分类属性赋值
                List<BaseCategoryView> category2And3List = category2MapEntry.getValue();
                List<JSONObject> category3List = category2And3List.stream().map(baseCategoryView -> {
                    JSONObject category3 = new JSONObject();
                    category3.put("categoryId", baseCategoryView.getCategory3Id());
                    category3.put("categoryName", baseCategoryView.getCategory3Name());
                    return category3;
                }).collect(Collectors.toList());
                //将三级分类集合 放入二级分类对象属性"categoryChild"
                category2.put("categoryChild",category3List);
            }
            //循环完之后 将二级分类集合放到一级分类对象"categoryChild" 前端这样定义的
            category1.put("categoryChild",category2List);
        }
        return resultList;
    }
}
