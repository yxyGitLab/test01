package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.SkuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 库存单元表 业务接口类
 * @author tomcat
 * @since 2023-06-09
 */
public interface SkuInfoService extends IService<SkuInfo> {

}
