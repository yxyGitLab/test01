package com.atguigu.gmall.product.service;

import com.atguigu.gmall.product.model.BaseSaleAttr;
import com.atguigu.gmall.product.model.SpuInfo;
import com.atguigu.gmall.product.model.SpuPoster;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

public interface SpuManageService {
    /**
     *  商品SPU分页查询
     * @param spuInfoPage MP分页对象
     * @param category3Id 分类id
     * @return
     */
    Page<SpuInfo> getSpuByPage(Page<SpuInfo> spuInfoPage, Long category3Id);

    /**
     * 查询当前平台包含所有销售属性列表
     *
     * @return
     */
    List<BaseSaleAttr> getBaseSaleAttrList();
    /**
     * 保存商品SPU信息
     *
     * @param spuInfo
     * @return
     */
    void saveSpuInfo(SpuInfo spuInfo);
    /**
     * 根据商品SpuId查询海报图片列表
     * @param spuId
     * @return
     */
    List<SpuPoster> getSpuPosterBySpuId(Long spuId);
}
