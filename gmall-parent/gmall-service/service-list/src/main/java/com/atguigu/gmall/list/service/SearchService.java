package com.atguigu.gmall.list.service;


import com.atguigu.gmall.list.model.SearchParam;
import com.atguigu.gmall.list.model.SearchResponseVo;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;

public interface SearchService {
    /**
     * 将指定SKU商品导入索引库
     * @param skuId
     */
    void upperGoods(Long skuId);

    /**
     * 将指定商品文档删除
     * @param skuId
     */
    void lowerGoods(Long skuId);

    /**
     * 更新商品热度分值
     * @param skuId
     */
    void incrHotScore(Long skuId);
    /**
     * 提供给前端服务/移动端,商品检索
     *
     * @param searchParam
     * @return
     */
    SearchResponseVo search(SearchParam searchParam);
    /**
     * 封装DSL语句 检索请求地址
     * @param searchParam
     * @return
     */

    SearchRequest buildDSL(SearchParam searchParam);

    SearchResponseVo parseResult(SearchResponse searchResponse, SearchParam searchParam);

}
