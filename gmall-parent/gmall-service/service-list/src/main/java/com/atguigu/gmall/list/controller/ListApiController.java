package com.atguigu.gmall.list.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.list.model.Goods;
import com.atguigu.gmall.list.model.SearchParam;
import com.atguigu.gmall.list.model.SearchResponseVo;
import com.atguigu.gmall.list.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/list")
@Slf4j
public class ListApiController {


    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;


    @Autowired
    private SearchService searchService;


    /**
     * 快速创建索引库,设置索引库映射信息(根据实体类上ES相关注解获取)
     * @return
     */
    @GetMapping("/inner/createIndex")
    public Result createIndex(){
        //1.创建索引库
        elasticsearchRestTemplate.createIndex(Goods.class);
        //2.设置索引库石映射信息
        elasticsearchRestTemplate.putMapping(Goods.class);
        return Result.ok();
    }



    /**
     * 删除索引库
     * @return
     */
    @GetMapping("/inner/deleteIndex")
    public Result deleteIndex() {
        elasticsearchRestTemplate.deleteIndex("goods");
        return Result.ok();
    }


    /**
     * 该接口仅用于测试:完成将指定商品导入索引库
     * @param skuId
     * @return
     */
    @GetMapping("/inner/upperGoods/{skuId}")
    public Result upperGoods(@PathVariable("skuId") Long skuId){
        searchService.upperGoods(skuId);
        return Result.ok();
    }

    /**
     * 测试接口，商品文档删除
     * @param skuId
     * @return
     */
    @GetMapping("/inner/lowerGoods/{skuId}")
    public Result lowerGoods(@PathVariable("skuId") Long skuId){
        searchService.lowerGoods(skuId);
        return Result.ok();
    }


    /**
     * 更新商品热度分值
     * @param skuId
     */
    @GetMapping("/inner/incrHotScore/{skuId}")
    public void incrHotScore(@PathVariable("skuId") Long skuId){
        searchService.incrHotScore(skuId);
    }

    /**
     * 提供给前端服务/移动端,商品检索
     *
     * @param searchParam
     * @return
     */
    @PostMapping({"", "/"})
    public Result search(@RequestBody SearchParam searchParam,@RequestHeader(required = false, value = "userId") String userId) {
        log.info("获取到网关穿透用户ID：{}",userId);
        SearchResponseVo responseVo = searchService.search(searchParam);
        return Result.ok(responseVo);
    }

}
