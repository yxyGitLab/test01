package com.atguigu.gmall.cart.controller;

import com.atguigu.gmall.cart.model.CartInfo;
import com.atguigu.gmall.cart.service.CartService;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/cart")
public class CartController {
        @Autowired
        private CartService cartService;
    /**
     * 将"用户"(包括临时用户或者真实用户)选择商品加入到购物车
     * @param skuId 商品ID
     * @param num   购买数量
     * @return
     */
    @GetMapping("/addToCart/{skuId}/{num}")
    public Result addToCart(HttpServletRequest request, @PathVariable("skuId") Long skuId,
                            @PathVariable("num") Integer num) {
        //1.优先获取真实用户ID
        String userId = AuthContextHolder.getUserId(request);
        //2.如果真实用户ID为空情况 尝试获取临时户用户
        if (StringUtils.isBlank(userId)) {
            userId = AuthContextHolder.getUserTempId(request);
        }
        //3.调用业务逻辑层完成加购操作
        cartService.addToCart(userId, skuId, num);
        return Result.ok();
    }

    /**
     * 查询当前用户(登录用户,临时用户)购物车商品列表
     * @param request
     * @return
     */
    @GetMapping("/cartList")
    public Result getCartList(HttpServletRequest request) {
        //获取用户ID(真实用户ID或者临时用户ID)
        //1.优先使用登陆后用户ID
        String userId = AuthContextHolder.getUserId(request);
        //2.用户未登录再获取临时用户ID
        String userTempId = AuthContextHolder.getUserTempId(request);
        //3.调用业务层获取购物车列表数据
        List<CartInfo> cartInfoList = cartService.getCartList(userId, userTempId);
        return Result.ok(cartInfoList);
    }

    /**
     * 修改购物车商品选中状态
     * @param skuId
     * @param ischecked
     * @return
     */
    @GetMapping("/checkCart/{skuId}/{isChecked}")
    public Result updateCheckState(HttpServletRequest request, @PathVariable("skuId") Long skuId,
                                   @PathVariable("isChecked") int ischecked){
        //1.优先获取真实用户ID
        String userId = AuthContextHolder.getUserId(request);
        //2.如果真实用户ID为空情况 尝试获取临时户用户
        if (StringUtils.isBlank(userId)) {
            userId = AuthContextHolder.getUserTempId(request);
        }
        cartService.updateCheckState(userId, skuId, ischecked);
        return Result.ok();
    }

    /**
     * 删除指定购物车商品
     * @param skuId
     * @return
     */
    @DeleteMapping("/deleteCart/{skuId}")
    public Result deleteCart(HttpServletRequest request, @PathVariable("skuId") Long skuId){
        //1.优先获取真实用户ID
        String userId = AuthContextHolder.getUserId(request);
        //2.如果真实用户ID为空情况 尝试获取临时户用户
        if (StringUtils.isBlank(userId)) {
            userId = AuthContextHolder.getUserTempId(request);
        }
        cartService.deleteCart(userId, skuId);
        return Result.ok();
    }

    /**
     * 查询指定用户购物车中选中商品列表
     * @param userId
     * @return
     */
    @GetMapping("/getCartCheckedList/{userId}")
    public List<CartInfo> getCartCheckedList(@PathVariable("userId") Long userId){
        //通过用户id动态获取
        return cartService.getCartCheckedList(userId);
    }
}