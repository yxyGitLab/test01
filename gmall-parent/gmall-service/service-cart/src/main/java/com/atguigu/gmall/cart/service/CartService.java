package com.atguigu.gmall.cart.service;

import com.atguigu.gmall.cart.model.CartInfo;

import java.util.List;

public interface CartService {
    /**
     * 将"用户"(包括临时用户或者真实用户)选择商品加入到购物车
     * @param skuId 商品ID
     * @param num   购买数量
     * @param userId 用户id
     * @return
     */
    void addToCart(String userId, Long skuId, Integer num);
    /**
     * 查询购物车列表(合并购物车)
     * @param userId
     * @param userTempId
     * @return
     */
    List<CartInfo> getCartList(String userId, String userTempId);
    /**
     * 修改购物车商品选中状态
     * @param skuId
     * @param ischecked
     * @return
     */
    void updateCheckState(String userId, Long skuId, int ischecked);
    /**
     * 删除指定购物车商品
     * @param skuId
     * @return
     */
    void deleteCart(String userId, Long skuId);
    /**
     * 查询指定用户购物车中选中商品列表
     * @param userId
     * @return
     */
    List<CartInfo> getCartCheckedList(Long userId);
}
