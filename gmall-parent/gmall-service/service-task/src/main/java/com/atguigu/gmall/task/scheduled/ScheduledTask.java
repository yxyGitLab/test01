package com.atguigu.gmall.task.scheduled;

import com.atguigu.gmall.common.rabbit.config.MqConst;
import com.atguigu.gmall.common.rabbit.util.RabbitService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务类
 */
@Slf4j
@Component
@EnableScheduling //开启定时任务
public class ScheduledTask {

    @Autowired
    private RabbitService rabbitService;


    /**
     * cron表达式 出处:定时任务框架Quartz
     * 定时任务逻辑:
     * 每日凌晨1点执行 秒杀商品预热消息发送  测试每隔5秒触发一次
     * 开发阶段:每隔30秒触发一次
     */
    //@Scheduled(cron = "0 0 1 * * ?")
    //@Scheduled(cron = "0/30 * * * * ?")
    @XxlJob("sendSeckillGoodsUpper")
    public void sendSeckillGoodsUpper() {
        log.info("任务执行..........");
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_TASK, MqConst.ROUTING_TASK_1, "");
    }

    /**
     * 定时清理秒杀数据
     * 正式:每日下午6点执行
     * 测试:每隔5秒执行一次
     */
    @XxlJob("sendSeckillGoodsClean")
    public void sendSeckillGoodsClean() {
        log.info("任务执行..........");
        rabbitService.sendMessage(MqConst.EXCHANGE_DIRECT_TASK, MqConst.ROUTING_TASK_18, "");
    }
}
