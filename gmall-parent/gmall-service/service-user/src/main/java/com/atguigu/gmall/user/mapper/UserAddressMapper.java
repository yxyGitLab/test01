package com.atguigu.gmall.user.mapper;

import com.atguigu.gmall.user.model.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户地址表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-20
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
