package com.atguigu.gmall.user.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "地区")
public class AreaInfo {

    @ApiModelProperty("省份")
    private String province = "";

    @ApiModelProperty("城市")
    private String city = "";


}
