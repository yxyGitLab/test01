package com.atguigu.gmall.user.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "ip与地区映射")
public class IpAreaRes {

    @ApiModelProperty("详细地址信息")
    private String address;

    @ApiModelProperty("结构信息")
    private Content content;

    @ApiModelProperty("结果状态返回码")
    private int status;

    @Data
    public static class Content {
        @ApiModelProperty("详细地址信息")
        private AddressDetail address_detail;
        @ApiModelProperty("简要地址信息")
        private String address;
        @ApiModelProperty("经纬度")
        private Point point;
    }

    @Data
    @ApiModel(description = "详细地址信息")
    public static class AddressDetail {
        @ApiModelProperty("省份")
        private String province;
        @ApiModelProperty("城市")
        private String city;
        private String district;
        private String street;
        private String street_number;
        @ApiModelProperty("百度城市代码")
        private int city_code;
    }

    @Data
    @ApiModel(description = "经纬度")
    public static class Point {
        @ApiModelProperty("当前城市中心点经度")
        private String x;
        @ApiModelProperty("当前城市中心点维度")
        private String y;

    }


}
