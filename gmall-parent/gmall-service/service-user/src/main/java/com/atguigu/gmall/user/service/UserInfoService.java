package com.atguigu.gmall.user.service;

import com.atguigu.gmall.user.model.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 用户表 业务接口类
 * @author tomcat
 * @since 2023-06-20
 */
public interface UserInfoService extends IService<UserInfo> {
    /**
     * 实现用户的登录
     * @param loginVo
     * @param request
     * @return
     */
    Map<String, Object> login(UserInfo loginVo, HttpServletRequest request);
    /**
     * 退出系统 拿到客户端提交Token,请求头携带Cookie
     * @return
     */
    void logout(String token);
}
