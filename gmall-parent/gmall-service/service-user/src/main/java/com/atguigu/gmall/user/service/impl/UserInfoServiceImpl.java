package com.atguigu.gmall.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.common.util.IpUtil;
import com.atguigu.gmall.user.model.UserInfo;
import com.atguigu.gmall.user.mapper.UserInfoMapper;
import com.atguigu.gmall.user.service.UserInfoService;
import com.atguigu.gmall.user.util.AreaInfo;
import com.atguigu.gmall.user.util.BaiDuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 用户表 业务实现类
 *
 * @author tomcat
 * @since 2023-06-20
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

   @Autowired
   private RedisTemplate redisTemplate;

    /**
     * 1.获取认证信息中账号，查询账号信息是否存在
     * 2.判断用户提交的密码是否与数据库存的密文是否匹配
     * 3.验证通过生成UUID令牌
     * 4.将令牌作为key,用户信息作为value，存入redis,设置key过期时间24小时
     * 5.按照接口文档响应业务数据（包含令牌跟用户信息）
     * @param loginVo 认证信息中的  参数名称LoginName中的值可能是一个手机号，账号，邮箱号，
     * @param request
     * @return
     */
    @Override
    public Map<String, Object> login(UserInfo loginVo, HttpServletRequest request) {
        //1.获取认证信息中账号，查询账号信息是否存在
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(loginVo.getLoginName()) && StringUtils.isNotBlank(loginVo.getPasswd())){
            //
            queryWrapper.eq(UserInfo::getLoginName,loginVo.getLoginName())
                    .or().eq(UserInfo::getEmail,loginVo.getLoginName())
                    .or().eq(UserInfo::getPhoneNum,loginVo.getLoginName());
            //从数据库中查询的UserInfo
            UserInfo userInfo = this.getOne(queryWrapper);
            //2.判断用户提交的密码是否与数据库存的密文是否匹配
            //2.1判断数据库查询回来的数据是否存在
            if(userInfo==null){
                throw new RuntimeException("账号错误!");
            }
            //2.2对提交的明文密码进行MD5加密 userInfo封装了从数据库中查回来的数据
            String userLoginPwd = DigestUtils.md5DigestAsHex(loginVo.getPasswd().getBytes());
            if(!userInfo.getPasswd().equals(userLoginPwd)){
                throw new RuntimeException("密码错误!");
            }
            //3.验证通过生成UUID令牌
            String token = UUID.randomUUID().toString().replaceAll("-", "");
            //4.将令牌作为key,用户信息作为value，存入redis,设置key过期时间24小时
            JSONObject redisValueMap = new JSONObject();
            redisValueMap.put("id",userInfo.getId());
            //得到Ip地址所在地 做异地登录
            AreaInfo area = BaiDuService.getArea(IpUtil.getIpAddress(request));
            if(area!=null){
                //存用户所在的城市信息
                redisValueMap.put("city",area.getCity());//异地登录给用户进行预警短信，引导进行再次登录
            }

            redisValueMap.put("mac","666");//设备唯一标识 由前端提交
            //设置过期时间24小时
            String key="user:"+token;
            redisTemplate.opsForValue().set(key,redisValueMap,24, TimeUnit.HOURS);
            //5.按照接口文档响应业务数据（包含令牌跟用户信息）
            HashMap<String, Object> loginResult = new HashMap<>();
            loginResult.put("token",token);

            loginResult.put("nickName",userInfo.getNickName());
            return loginResult;
        }
        return null;
    }
    /**
     * 退出系统 拿到客户端提交Token,请求头携带Cookie
     * @return
     */
    @Override
    public void logout(String token) {
        //需要拿到token值，在请求头中获取
        String key = "user:"+token;
        redisTemplate.delete(key);
    }
}
