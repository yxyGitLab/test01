package com.atguigu.gmall.user.service.impl;

import com.atguigu.gmall.user.model.UserAddress;
import com.atguigu.gmall.user.mapper.UserAddressMapper;
import com.atguigu.gmall.user.service.UserAddressService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户地址表 业务实现类
 *
 * @author tomcat
 * @since 2023-06-20
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService {
    /**
     * 根据用户ID查询用户收件地址列表
     * @param userId
     * @return
     */
    @Override
    public List<UserAddress> getUserAddressListByUserId(Long userId) {
        LambdaQueryWrapper<UserAddress> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserAddress::getUserId, userId);
        return this.list(queryWrapper);
    }
}
