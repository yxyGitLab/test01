package com.atguigu.gmall.user.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class TestUserDTO {
    private Long id;

    @NotNull(message = "地址为必填项")
    private String address;
    @NotNull(message = "邮箱为必填项")
    @Email(message = "邮箱格式非法")
    private String email;
    @NotNull(message = "年龄不能为空")
    @Range(min = 0, max = 150, message = "年龄区间只能在0-150之间")
    private Integer age;
    @NotNull(message = "手机号为必填项")
    @Pattern(regexp = "1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\\d{8}$", message = "手机号格式非法")
   private String mobile;
}
