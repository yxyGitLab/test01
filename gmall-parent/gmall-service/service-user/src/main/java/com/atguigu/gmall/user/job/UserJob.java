package com.atguigu.gmall.user.job;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class UserJob {
    /**
     * 每月1号发送 信用卡账单 或者 贷款
     * 给10W会员发送营销短信
     */
    @XxlJob("sendSms")
    public void sendSms(){
// 可参考Sample示例执行器中的示例任务"ShardingJobHandler"了解试用
        int shardTotal = XxlJobHelper.getShardTotal();
        log.info("总执行器数量/分片数量:{}", shardTotal);
        int shardIndex = XxlJobHelper.getShardIndex();
        log.info("当前执行器索引:{}", shardIndex);
    }
}
