package com.atguigu.gmall.user.service;

import com.atguigu.gmall.user.model.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户地址表 业务接口类
 * @author tomcat
 * @since 2023-06-20
 */
public interface UserAddressService extends IService<UserAddress> {
    /**
     * 根据用户ID查询用户收件地址列表
     * @param userId
     * @return
     */
    List<UserAddress> getUserAddressListByUserId(Long userId);

}
