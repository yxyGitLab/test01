package com.atguigu.gmall.mq.controller;

import com.atguigu.gmall.common.rabbit.util.RabbitService;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.mq.config.DeadLetterMqConfig;
import com.atguigu.gmall.mq.config.DelayedMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/admin/mq")
public class MqController {

    @Autowired
    private RabbitService rabbitService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送普通消息
     *
     * @param msg
     * @return
     */
    @GetMapping("/sendMsg")
    public Result sendMsg(@RequestParam("msg") String msg) {
        String exchange = "exchange.confirm";
        String routingKey = "routing.confirm";
        rabbitService.sendMessage(exchange, routingKey, msg);
        return Result.ok();
    }

    /**
     * 采用死信实现延迟消息
     *
     * @param msg
     * @return
     */
    @GetMapping("/sendDeadLetterMsg")
    public Result sendDeadLetterMsg(String msg) {
        rabbitTemplate.convertAndSend(DeadLetterMqConfig.exchange_dead, DeadLetterMqConfig.routing_dead_1, msg);
        log.info("基于死信发送延迟消息成功:{}", msg);
        return Result.ok();
    }

    /**
     * 发送延迟消息-采用延迟插件实现
     *
     * @param msg
     * @param delayTime 延迟时间 单位:s
     * @return
     */
    @GetMapping("/sendDelayMsg")
    public Result sendDelayMsg(@RequestParam("msg") String msg, @RequestParam("delayTime") Integer delayTime) {
        String exchange = DelayedMqConfig.exchange_delay;
        String routingKey = DelayedMqConfig.routing_delay;
        rabbitService.sendDelayMessage(exchange, routingKey, msg, delayTime);
        return Result.ok();
    }
}