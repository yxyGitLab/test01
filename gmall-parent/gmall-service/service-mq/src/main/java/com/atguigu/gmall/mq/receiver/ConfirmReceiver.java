package com.atguigu.gmall.mq.receiver;

import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.amqp.core.Message;

@Slf4j
@Component
public class ConfirmReceiver {

    @SneakyThrows //自动 try catch
    @RabbitListener(
            bindings = @QueueBinding(
                    exchange = @Exchange("exchange.confirm"),
                    value = @Queue("confirm.queue"),
                    key = "routing.confirm"
            )
    )
    public void receiverConfirmMessage(String data, Channel channel, Message message) {
        if(StringUtils.isNotBlank(data)){
            log.info("消费者接收到消息:{},模拟执行业务", data);
        }

        //如果开启手动确认, 参数一:投递消息消息标签  参数二:是否批量确认消息
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}