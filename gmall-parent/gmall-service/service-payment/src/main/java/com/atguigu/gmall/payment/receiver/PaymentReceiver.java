package com.atguigu.gmall.payment.receiver;

import com.atguigu.gmall.common.rabbit.config.MqConst;
import com.atguigu.gmall.enums.model.PaymentStatus;
import com.atguigu.gmall.enums.model.PaymentType;
import com.atguigu.gmall.payment.model.PaymentInfo;
import com.atguigu.gmall.payment.service.AlipayService;
import com.atguigu.gmall.payment.service.PaymentInfoService;
import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class PaymentReceiver {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private PaymentInfoService paymentInfoService;

    @Autowired
    private AlipayService alipayService;


    /**
     * 监听关闭订单消息,关闭交易记录(本地跟支付宝)
     * @param orderId
     * @param message
     * @param channel
     */
    @SneakyThrows
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = MqConst.EXCHANGE_DIRECT_PAYMENT_CLOSE, durable = "true"),
            //声明队列
            value = @Queue(value = MqConst.QUEUE_PAYMENT_CLOSE, durable = "true"),
            key = MqConst.ROUTING_PAYMENT_CLOSE
    ))
    public void processClosePayment(Long orderId, Message message, Channel channel) {
        if (orderId != null) {
            log.info("[支付服务]监听到关闭交易消息:{}", orderId);
            //1.消息幂等性 set nx
            String key = "mq:" + orderId + ":close";
            Boolean flag = redisTemplate.opsForValue().setIfAbsent(key, orderId, 5, TimeUnit.SECONDS);
            if (!flag) {
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
                return;
            }
            //2.关闭交易记录
            //2.1 根据订单ID查询本地交易记录
            PaymentInfo paymentInfo = paymentInfoService.getPayInfoByOrderIdOrOutTradeNo(orderId.toString(), PaymentType.ALIPAY.name());
            //如果未支付
            if (paymentInfo != null && PaymentStatus.UNPAID.name().equals(paymentInfo.getPaymentStatus())) {
                //2.2 修改本地交易记录状态
                paymentInfo.setPaymentStatus(PaymentStatus.CLOSED.name());
                paymentInfoService.updateById(paymentInfo);
                //2.3 todo 查询支付宝端交易状态
                String aliPaySatus = alipayService.getAliPaySatus(paymentInfo.getOutTradeNo());
                //如果状态等于了响应的参数
                if ("WAIT_BUYER_PAY".equals(aliPaySatus)) {
                    //2.4 todo 如果支付宝交易状态:等待买家付款(超时未支付),关闭支付宝端交易
                    alipayService.closeAlipay(paymentInfo.getOutTradeNo());
                }
            }
        }
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
