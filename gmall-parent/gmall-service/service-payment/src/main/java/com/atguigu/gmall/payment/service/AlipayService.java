package com.atguigu.gmall.payment.service;

import com.atguigu.gmall.order.model.OrderInfo;

import java.util.Map;

public interface AlipayService {
    /**
     * 渲染支付页面
     * @param orderId
     * @return
     */
    String createPlipayPage(Long orderId);
    /**
     * 处理支付宝异步回调：通知商户系统支付结果
     * @param map
     * @return
     */
    String paySuccessNotify(Map map);

    /**
     * 退款接口
     * @param orderId
     * @return
     */
    void refund(Long orderId);

    /**
     * 根据商户订单编号查询支付宝端交易状态
     * @param outTradeNo
     * @return
     */
    String getAliPaySatus(String outTradeNo);
    /**
     * 关闭支付宝交易订单
     * @param outTradeNo
     */
    void closeAlipay(String outTradeNo);

}
