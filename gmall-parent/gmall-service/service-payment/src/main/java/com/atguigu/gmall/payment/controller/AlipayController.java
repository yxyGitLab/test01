package com.atguigu.gmall.payment.controller;


import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.payment.config.AlipayConfig;
import com.atguigu.gmall.payment.service.AlipayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


@RestController
@RequestMapping("/api/payment/")
public class AlipayController {


    @Autowired
    private AlipayService alipayService;

    /**
     * 渲染支付页面
     * @param orderId
     * @return
     */
    @GetMapping("/alipay/submit/{orderId}")
    public String alipayPage(@PathVariable("orderId") Long orderId) {
        //2.调用业务层获取支付页面
        return alipayService.createPlipayPage(orderId);
    }

    /**
     * 支付宝同步回调地址处理,重定向地址支付成功页面
     * @param response
     */
    @GetMapping("/alipay/callback/return")
    public void redirectUrl(HttpServletResponse response) throws IOException {
        //重定向地址:http://payment.gmall.com/pay/success.html
        response.sendRedirect(AlipayConfig.return_order_url);
    }

    /**
     * 处理支付宝异步回调：通知商户系统支付结果
     * @param map
     * @return
     */
    @PostMapping("/alipay/callback/notify")
    public String paySuccessNotify(@RequestParam Map map){
        return alipayService.paySuccessNotify(map);
    }

    /**
     * 退款接口
     * @param orderId
     * @return
     */
    @GetMapping("/alipay/refund/{orderId}")
    public Result refund(@PathVariable("orderId") Long orderId) {
        alipayService.refund(orderId);
        return Result.ok();
    }
}