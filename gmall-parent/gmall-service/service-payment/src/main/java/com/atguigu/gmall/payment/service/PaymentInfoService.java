package com.atguigu.gmall.payment.service;

import com.atguigu.gmall.payment.model.PaymentInfo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface PaymentInfoService extends IService<PaymentInfo> {


    /**
     * 保存本地交易记录
     * @param paymentInfo 支付对象
     * @param paymentType 支付类型
     */
    public void savePaymentInfo(PaymentInfo paymentInfo, String paymentType);

    /**
     * 根据订单编号+支付方式，查询本地交易记录
     * @param outTradeNo
     * @param name
     * @return
     */
    PaymentInfo getPaymentInfo(String outTradeNo, String name);
    /**
     * 根据订单编号/订单ID +支付方式 查询本地交易记录
     * @param orderId
     * @param paymentType
     * @return
     */
    PaymentInfo getPayInfoByOrderIdOrOutTradeNo(String orderId, String paymentType);
}