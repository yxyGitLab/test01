package com.atguigu.gmall.order.mapper;

import com.atguigu.gmall.order.model.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单明细表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-25
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
