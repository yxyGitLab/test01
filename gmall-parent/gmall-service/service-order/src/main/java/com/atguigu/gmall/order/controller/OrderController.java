package com.atguigu.gmall.order.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.order.model.OrderInfo;
import com.atguigu.gmall.order.service.OrderInfoService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderInfoService orderInfoService;


    /**
     * 渲染订单确认页面相关数据汇总
     * @return
     */
    @GetMapping("/auth/trade")
    public Result<Map> orderTradeData(HttpServletRequest request) {
        //1.获取登录用户ID,购物车有临时用户，但其他模块都是真实用户
        String userId = AuthContextHolder.getUserId(request);
        Map<String, Object> mapResult = orderInfoService.orderTradeData(userId);
        return Result.ok(mapResult);
    }

    /**
     * 提交普通商城订单
     * @param orderInfo
     * @return
     */
    @PostMapping("/auth/submitOrder")
    public Result submitOrder(HttpServletRequest request, @RequestBody OrderInfo orderInfo,
                              @RequestParam("tradeNo") String tradeNo){
        //1.获取用户ID
        String userId = AuthContextHolder.getUserId(request);
        orderInfo.setUserId(Long.valueOf(userId));
        //2.提交订单
        Long orderId = orderInfoService.submitOrder(orderInfo, tradeNo);
        return Result.ok(orderId);
    }

    /**
     * 分页获取用户订单列表(包含订单明细)
     * @param request
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("/auth/{page}/{limit}")
    public Result getOrderByPage(HttpServletRequest request, @PathVariable("page") Long page, @PathVariable("limit") Long limit) {
        //1.获取登录用户ID
        String userId = AuthContextHolder.getUserId(request);
        //2.构建分页对象
        Page<OrderInfo> pageInfo = new Page<>(page, limit);
        //3.调用业务层查询
        pageInfo = orderInfoService.getOrderByPage(pageInfo, userId);
        return Result.ok(pageInfo);
    }

    /**
     * 查询订单信息(包含订单明细)
     * @param orderId
     * @return
     */
    @GetMapping("/inner/getOrderInfo/{orderId}")
    public OrderInfo getOrderInfo(@PathVariable("orderId") Long orderId){
        return orderInfoService.getOrderInfo(orderId);
    }

    /**
     * 提供给库存系统同步调用拆单业务接口
     * @param orderId 订单ID
     * @param wareSkuMap 仓库跟商品对照关系
     * @return
     */
    @PostMapping("/orderSplit")
    public String orderSplit(@RequestParam("orderId") Long orderId, @RequestParam("wareSkuMap") String wareSkuMap){
        return orderInfoService.orderSplit(orderId, wareSkuMap);
    }

    /**
     * 保存秒杀订单
     * @param orderInfo
     * @return
     */
    @PostMapping("/inner/seckill/submitOrder")
    public Long submitSeckillOrder(@RequestBody OrderInfo orderInfo){
        return orderInfoService.submitSeckillOrder(orderInfo);
    }

}