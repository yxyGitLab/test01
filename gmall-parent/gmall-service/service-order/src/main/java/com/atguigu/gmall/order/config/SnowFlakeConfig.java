package com.atguigu.gmall.order.config;

import com.atguigu.gmall.common.util.SnowFlake;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SnowFlakeConfig {

    @Bean
    public SnowFlake snowFlake(){
        return new SnowFlake(1,1,1);
    }

}