package com.atguigu.gmall.order.mapper;

import com.atguigu.gmall.order.model.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * 订单表 订单表 Mapper 接口
 *
 * @author tomcat
 * @since 2023-06-25
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {
    /**
     * MP动态SQL实现分页,只需要传入Page分页对象,MP框架会自动拼接limit关键字
     * @param pageInfo
     * @param userId
     * @return
     */
    Page<OrderInfo> getOrderByPage(Page<OrderInfo> pageInfo, String userId);
}
