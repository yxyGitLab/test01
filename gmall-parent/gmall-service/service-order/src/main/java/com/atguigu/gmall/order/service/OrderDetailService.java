package com.atguigu.gmall.order.service;

import com.atguigu.gmall.order.model.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 订单明细表 业务接口类
 * @author tomcat
 * @since 2023-06-25
 */
public interface OrderDetailService extends IService<OrderDetail> {

}
