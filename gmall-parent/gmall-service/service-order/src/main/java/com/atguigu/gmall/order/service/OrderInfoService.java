package com.atguigu.gmall.order.service;

import com.atguigu.gmall.enums.model.ProcessStatus;
import com.atguigu.gmall.order.model.OrderInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 订单表 订单表 业务接口类
 * @author tomcat
 * @since 2023-06-25
 */
public interface OrderInfoService extends IService<OrderInfo> {
    /**
     * 渲染订单确认页面相关数据汇总
     * @return
     */
    Map<String, Object> orderTradeData(String userId);
    /**
     * 提交普通商城订单
     * @param orderInfo
     *       tradeNo用户提交的流水号
     * @return
     */
    Long submitOrder(OrderInfo orderInfo, String tradeNo);
    /**
     * 保存订单,以及订单明细
     *
     * @param orderInfo
     * @param sourceType 订单来源  1:普通商城订单  2：秒杀订单
     * @return
     */
    Long saveOrder(OrderInfo orderInfo,String sourceType);





    /**
     * 生成流水号
     * @param userId
     * @return
     */
    String generateTradeNo(String userId);


    /**
     * 验证流水号是否一致
     * @param userId
     * @param tradeNo
     * @return
     */
    Boolean checkTradeNo(String userId, String tradeNo);


    /**
     * 删除业务流水号
     * @param userId
     */
    void deleteTradeNo(String userId);
    /**
     * 分页获取用户订单列表(包含订单明细)
     * @return
     */
    Page<OrderInfo> getOrderByPage(Page<OrderInfo> pageInfo, String userId);

    /**
     * 将订单关闭
     * @param orderId
     */
    void execExpiredOrder(Long orderId);
    /**
     * 根据订单ID修改为指定订单状态
     *
     * @param orderId
     *
     */
    void updateOrderStatus(Long orderId, ProcessStatus closed);
    /**
     * 查询订单信息(包含订单明细)
     * @param orderId
     * @return
     */
    OrderInfo getOrderInfo(Long orderId);

    /**
     * 发送消息通知库存系统锁定商品库存
     * @param orderId
     */
    void sendLockStockMsg(Long orderId);
    /**
     * 提供给库存系统同步调用拆单业务接口
     * @param orderId 订单ID
     * @param wareSkuMap 仓库跟商品对照关系
     * @return
     */
    String orderSplit(Long orderId, String wareSkuMap);

    /**
     * 提交保存秒杀订单
     * @param orderInfo
     * @return
     */
    Long submitSeckillOrder(OrderInfo orderInfo);
}
