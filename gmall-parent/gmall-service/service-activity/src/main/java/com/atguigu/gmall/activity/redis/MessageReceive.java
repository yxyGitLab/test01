package com.atguigu.gmall.activity.redis;

import com.github.benmanes.caffeine.cache.Cache;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 监听到Redis发布订阅话题中消息后执行业务逻辑
 */
@Slf4j
@Component
public class MessageReceive {

    @Autowired
    private Cache<String, String> cache;

    /**
     * 监听话题中商品状态,修改本地缓存商品状态位
     *
     * @param state ""35:0""
     */
    public void receiveSeckillGoodState(String state) {
        if (StringUtils.isNotBlank(state)) {
            log.info("接收到Redis话题中消息:{}", state);
            state = state.replace("\"", "");
            //获取商品id
            String[] split = state.split(":");
            if (split != null && split.length == 2) {
                cache.put(split[0], split[1]);
            }
        }
    }
}
