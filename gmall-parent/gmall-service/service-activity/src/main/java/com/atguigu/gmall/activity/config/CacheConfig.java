package com.atguigu.gmall.activity.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class CacheConfig {


    /**
     * 用于缓存商品状态位本地缓存对象
     * @return
     */
    @Bean
    public Cache<String, String> seckillStatusCache() {
        return Caffeine.newBuilder()
                .maximumSize(1024)
                .expireAfterWrite(Duration.ofHours(24))
                .build();
    }


}
