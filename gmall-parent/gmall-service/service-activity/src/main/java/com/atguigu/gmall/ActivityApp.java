package com.atguigu.gmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class ActivityApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ActivityApp.class, args);
        RedisTemplate redisTemplate = applicationContext.getBean(RedisTemplate.class);

        //添加关闭服务器钩子 服务一旦停止,本地缓存数据无;但是分布式缓存中依然有数据
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("清理分布式缓存秒杀数据");
            Set<String> keys = redisTemplate.keys("seckill:" + "*");
            redisTemplate.delete(keys);
        }));
    }

}