package com.atguigu.gmall.activity.service;

import com.atguigu.gmall.activity.model.SeckillGoods;
import com.atguigu.gmall.activity.model.UserRecode;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.order.model.OrderDetail;
import com.atguigu.gmall.order.model.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 业务接口类
 */
public interface SeckillGoodsService extends IService<SeckillGoods> {

    /**
     * 查询当日参与秒杀商品
     * @return
     */
    List<SeckillGoods> getSeckillGoods();

    /**
     * 查询秒杀商品详情
     * @param skuId
     * @return
     */
    SeckillGoods getSecGoodsById(Long skuId);

    /**
     * 获取抢购码
     *
     * @param userId
     * @param skuId
     * @return
     */
    String getSeckillSkuIdStr(String userId, Long skuId);

    /**
     * 秒杀预下单,业务校验通过后,将秒杀请求放入消息队列,不等待秒杀业务处理立即返回
     * @param userId 用户ID
     * @param skuId 秒杀商品ID
     * @param buyCode 抢购码
     * @return
     */
    void seckillReq2Queue(String userId, Long skuId, String buyCode);

    /**
     * 秒杀核心业务处理 从Mq中处理秒杀请求
     * @param userRecode
     */
    void processSeckill(UserRecode userRecode);

    /**
     * 扣减秒杀商品库存
     * @param userRecode
     */
    void processDedcutStock(UserRecode userRecode);

    /**
     * 检查用户秒杀下单结果
     * @param userId
     * @param skuId
     * @return
     */
    Result checkSeckillResult(String userId, Long skuId);
    /**
     * 查询用户秒杀得到订单明细
     * @param
     * @return
     */
    Map<String,Object> getSeckillOrderDetail(String userId,Long skuId);

    /**
     * 保存秒杀订单 业务逻辑
     * @param orderInfo
     * @return
     */
    Long submitSeckillOrder(OrderInfo orderInfo);
    /**
     * 清理当日秒杀产生缓存
     */
    void processCleanCache();

}
