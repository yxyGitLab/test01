package com.atguigu.gmall.activity.controller;

import com.atguigu.gmall.activity.model.SeckillGoods;
import com.atguigu.gmall.activity.service.SeckillGoodsService;
import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.common.result.ResultCodeEnum;
import com.atguigu.gmall.common.util.AuthContextHolder;
import com.atguigu.gmall.order.model.OrderDetail;
import com.atguigu.gmall.order.model.OrderInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/activity")
public class SeckillGoodsController {


    //查询缓存
    @Autowired
    private SeckillGoodsService seckillGoodsService;


    /**
     * 查询当日参与秒杀商品
     *
     * @return
     */
    @GetMapping("/seckill/findAll")
    public Result getSeckillGoods() {
        List<SeckillGoods> list = seckillGoodsService.getSeckillGoods();
        return Result.ok(list);
    }

    /**
     * 查询秒杀商品详情  从redis获取
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/getSeckillGoods/{skuId}")
    public Result getSecGoodsById(@PathVariable("skuId") Long skuId) {
        SeckillGoods seckillGoods = seckillGoodsService.getSecGoodsById(skuId);
        return Result.ok(seckillGoods);
    }


    /**
     * 获取抢购码
     *
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/auth/getSeckillSkuIdStr/{skuId}")
    public Result getSeckillSkuIdStr(HttpServletRequest request, @PathVariable("skuId") Long skuId) {
        String userId = AuthContextHolder.getUserId(request);
        String buyCode = seckillGoodsService.getSeckillSkuIdStr(userId, skuId);
        return Result.ok(buyCode);
    }



    /**
     * 秒杀预下单,业务校验通过后,将秒杀请求放入消息队列,不等待秒杀业务处理立即返回
     * @param request
     * @param skuId 秒杀商品ID
     * @param buyCode 抢购码
     * @return
     */
    @PostMapping("/seckill/auth/seckillOrder/{skuId}")
    public Result seckillReq2Queue(HttpServletRequest request, @PathVariable("skuId") Long skuId, @RequestParam("skuIdStr") String buyCode) {
        String userId = AuthContextHolder.getUserId(request);
        seckillGoodsService.seckillReq2Queue(userId, skuId, buyCode);
        return Result.ok();
    }

    /**
     * 检查用户秒杀下单结果
     *
     * @param request
     * @param skuId
     * @return
     */
    @GetMapping("/seckill/auth/checkOrder/{skuId}")
    public Result checkSeckillResult(HttpServletRequest request, @PathVariable("skuId") Long skuId) {
        //1.获取当前登录用户
        String userId = AuthContextHolder.getUserId(request);
        if (StringUtils.isBlank(userId)) {
            throw new RuntimeException("未登录!");
        }
        //2.调用业务逻辑得到秒杀结果
        return seckillGoodsService.checkSeckillResult(userId, skuId);
    }



    /**
     * 渲染秒杀订单确认页面相关数据
     * @param request
     * @return
     */
    @GetMapping("/seckill/auth/trade/{skuId}")
    public Result<Map> seckillOrderTrade(HttpServletRequest request,@PathVariable("skuId") Long skuId){
        //1.获取当前登录用户
        String userId = AuthContextHolder.getUserId(request);
        if(StringUtils.isBlank(userId)){
            throw new RuntimeException("未登录");
        }
        //2.获取用户秒杀订单中订单明细
        Map<String,Object> mapResult=seckillGoodsService.getSeckillOrderDetail(userId,skuId);
        return Result.ok(mapResult);
    }

    /**
     * 保存秒杀订单
     * @param orderInfo
     * @return 跟以前支付对接 响应保存后秒杀订单ID
     */
    @PostMapping("/seckill/auth/submitOrder")
    public Result submitSeckillOrder(HttpServletRequest request, @RequestBody OrderInfo orderInfo){
        String userId = AuthContextHolder.getUserId(request);
        if (StringUtils.isBlank(userId)) {
            throw new RuntimeException("未登录！");
        }
        orderInfo.setUserId(Long.valueOf(userId));
        Long orderId = seckillGoodsService.submitSeckillOrder(orderInfo);
        return Result.ok(orderId);
    }
}
