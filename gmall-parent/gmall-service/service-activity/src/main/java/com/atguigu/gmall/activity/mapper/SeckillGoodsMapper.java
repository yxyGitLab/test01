package com.atguigu.gmall.activity.mapper;

import com.atguigu.gmall.activity.model.SeckillGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {

}
