package com.atguigu.gmall.activity.controller;

import com.github.benmanes.caffeine.cache.Cache;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

@RestController
@RequestMapping("/api/test/cache")
public class TestController {

    @Autowired
    private Cache<String, String> cache;

    /**
     * 用于测试查看本地缓存中商品状态
     *
     * @return
     */
    @GetMapping
    public Map getAllCache() {
        ConcurrentMap<@NonNull String, @NonNull String> asMap = cache.asMap();
        return asMap;
    }
}
