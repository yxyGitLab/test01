package com.atguigu.gmall.item.controller;

import com.atguigu.gmall.common.result.Result;
import com.atguigu.gmall.item.service.ItemService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    private ItemService itemService;


    /**
     * 查询商品信息,汇总详情页面需要数据
     * @param skuId
     * @return
     */
    @GetMapping("/{skuId}")
    public Result getItemInfo(@PathVariable("skuId") Long skuId) {
        Map<String, Object> itemData = itemService.getItemInfo(skuId);
        return Result.ok(itemData);
    }
}