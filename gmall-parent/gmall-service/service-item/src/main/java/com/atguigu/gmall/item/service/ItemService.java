package com.atguigu.gmall.item.service;

import java.util.Map;

public interface ItemService {
    /**
     * 查询商品信息,汇总详情页面需要数据
     * @param skuId
     * @return
     */
    Map<String, Object> getItemInfo(Long skuId);
}
