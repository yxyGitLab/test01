package com.atguigu.gmall.item.service.impl;

import com.atguigu.gmall.common.constant.RedisConst;
import com.atguigu.gmall.item.service.ItemService;
import com.atguigu.gmall.list.client.ListFeignClient;
import com.atguigu.gmall.product.client.ProductFeignClient;
import com.atguigu.gmall.product.model.*;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ProductFeignClient productFeignClient;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    @Autowired
    private ListFeignClient listFeignClient;

    /**
     * 远程调用商品微服务调用七项数据feign接口完成数据汇总
     * @param skuId
     * @return
     */
    @Override
    public Map<String, Object> getItemInfo(Long skuId) {
        //0.验证用户访问商品ID是否存在于布隆过滤器中  为了开发后续方便，将判断注释掉,到测试环境再取消注释
  /*      RBloomFilter<Long> bloomFilter = redissonClient.getBloomFilter(RedisConst.SKU_BLOOM_FILTER);
        if(!bloomFilter.contains(skuId)){
            //布隆过滤器确定有一定的误判率，只能用布隆过滤器判断元素可能存在或者一定不存在
            throw new RuntimeException("访问的商品不存在");
        }*/
        HashMap<String, Object> mapResult = new HashMap<>();

        //CompletableFuture 异步编排
        //适用于做较为复杂的业务处理
        //第一个异步任务对象
        CompletableFuture<SkuInfo> skuInfoCompletableFuture = CompletableFuture.supplyAsync(() -> {
            //1.根据SkuID查询Sku商品信息
            SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
            mapResult.put("skuInfo", skuInfo);
            return skuInfo;
        }, threadPoolExecutor);

        //thenAcceptAsync,是不需要返回值用这个方法，不加Async是由主线程执行
        //需要依赖skuInfoCompletableFuture 对象 执行
        CompletableFuture<Void> categoryViewCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //2.根据商品所属三级分类ID查询分类信息
            BaseCategoryView categoryView = productFeignClient.getCategoryView(skuInfo.getCategory3Id());
            mapResult.put("categoryView", categoryView);
        }, threadPoolExecutor);


        //3.根据SKuID查询Sku商品价格
        //发起异步任务
        CompletableFuture<Void> priceCompletableFuture = CompletableFuture.runAsync(() -> {
            BigDecimal skuPrice = productFeignClient.getSkuPrice(skuId);
            mapResult.put("price", skuPrice);
        }, threadPoolExecutor);


        CompletableFuture<Void> spuPosterCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //4.根据商品SpuID查询海报图片列表
            List<SpuPoster> spuPosterList = productFeignClient.getSpuPosterBySpuId(skuInfo.getSpuId());
            mapResult.put("spuPosterList", spuPosterList);
        }, threadPoolExecutor);


        //5.根据skuId查询当前SKU商品包含平台属性列表(包含属性值)
        //发起异步任务
        CompletableFuture<Void> skuAttrListCompletableFuture = CompletableFuture.runAsync(() -> {
            List<BaseAttrInfo> attrList = productFeignClient.getAttrList(skuId);
            mapResult.put("skuAttrList", attrList);
        }, threadPoolExecutor);


        CompletableFuture<Void> spuSaleAttrListCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //6.查询SPU商品所有销售属性,查询指定SKU销售属性选中效果
            List<SpuSaleAttr> spuSaleAttrList = productFeignClient.getSpuSaleAttrListCheckBySku(skuId, skuInfo.getSpuId());
            mapResult.put("spuSaleAttrList", spuSaleAttrList);
        }, threadPoolExecutor);


        CompletableFuture<Void> valuesSkuJsonCompletableFuture = skuInfoCompletableFuture.thenAcceptAsync(skuInfo -> {
            //7.在一组SPU商品下 切换不同SKU字符串
            String skuValueIdsMap = productFeignClient.getSkuValueIdsMap(skuInfo.getSpuId());
            mapResult.put("valuesSkuJson", skuValueIdsMap);
        }, threadPoolExecutor);

        //远程调用搜索服务更新商品热度
        CompletableFuture<Void> hotScoreCompletableFuture = CompletableFuture.runAsync(() -> {
            listFeignClient.incrHotScore(skuId);
        }, threadPoolExecutor);

        //8.汇总所有异步任务执行结果，要求每个任务都必须执行
        CompletableFuture.allOf(
                skuInfoCompletableFuture,
                categoryViewCompletableFuture,
                priceCompletableFuture,
                spuPosterCompletableFuture,
                skuAttrListCompletableFuture,
                spuSaleAttrListCompletableFuture,
                valuesSkuJsonCompletableFuture
        ).join();
        return mapResult;
    }
}

